const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const cardRouter = require('./src/routes/card.route');

const port = 8000;

app.listen(port, () => {
  console.log('Server is ready on port ' + port);
});

mongoose.connect('mongodb://localhost:27017/gwent', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB connection is ready'))
  .catch(err => console.log(err));

//Get the default connection
const db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app
  .use(bodyParser.json())
  .use(cardRouter);

module.export = mongoose;