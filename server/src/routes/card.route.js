const express = require('express');
const router = express.Router();
const cardController = require('../controllers/card.controller');

router.get('/cards', cardController.getAllCards);
router.get('/cards/:id', cardController.getCardById);
router.post('/cards', cardController.addCard);

module.exports = router;