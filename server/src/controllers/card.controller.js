const {
  Request,
  Response
} = require('express');
const mongoose = require('mongoose');
const Card = require('../models/card.model');

/**
 * @param {Request} req
 * @param {Response} res
 */
getAllCards = async (req, res) => {
  const cards = await Card.find().exec();
  res.status(200).json(cards);
}

/**
 * @param {Request} req
 * @param {Response} res
 */
getCardById = async (req, res) => {
  const id = req.params.id;
  try {
    const card = await Card.findById(id).exec();
    if (card) {
      return res.status(200).json(card);
    }
    res.status(401).json({
      message: 'No entry found with the given id'
    })
  } catch (err) {
    res.status(500).json({
      error: err
    })
  }
}

/**
 * @param {Request} req
 * @param {Response} res
 */
addCard = async (req, res) => {
  const card = new Card({
    _id: new mongoose.Types.ObjectId(),
    name: 'test adding',
  });

  try {
    const result = await card.save();
    res.status(201).json({
      message: 'success',
      createdCard: result,
    })
  } catch (err) {
    console.log(err);
  };

}

module.exports = {
  getAllCards,
  getCardById,
  addCard,
};