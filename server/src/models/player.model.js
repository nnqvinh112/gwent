const mongoose = require('mongoose');

const cardSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  description: String,
  price: Number,
  imageUrl: String,
  unitClass: String,
  damage: Number,
  isHero: Boolean,
});

const Card = mongoose.model('card', cardSchema);

module.exports = Card;