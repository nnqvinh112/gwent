import {
  FILTER_CARDS,
  LOAD_ALL_CARDS_ERROR,
  LOAD_ALL_CARDS_REQUEST,
  LOAD_ALL_CARDS_SUCCESS,
} from '../actions/card.action'

const INITIAL_STATE = {
  isLoading: false,
  repos: []
}

function cardReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_ALL_CARDS_REQUEST:
      return {
        isLoading: true,
      }
    case LOAD_ALL_CARDS_ERROR:
      return {
        isLoading: false,
        message: 'Failed to load cards',
        repos: [],
      }
    case LOAD_ALL_CARDS_SUCCESS:
      return {
        isLoading: false,
        repos: action.payload || [],
      }
    case FILTER_CARDS:
      return state.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      })
    default:
      return state
  }
}

export default cardReducer