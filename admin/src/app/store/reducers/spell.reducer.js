import {
  LOAD_ALL_SPELLS_ERROR,
  LOAD_ALL_SPELLS_REQUEST,
  LOAD_ALL_SPELLS_SUCCESS,
} from '../actions/spell.action'

const INITIAL_STATE = {
  isLoading: false,
  repos: []
}

function spellReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_ALL_SPELLS_REQUEST:
      return {
        isLoading: true,
      };
    case LOAD_ALL_SPELLS_ERROR:
      return {
        isLoading: false,
          message: 'Failed to load spells',
          repos: [],
      };
    case LOAD_ALL_SPELLS_SUCCESS:
      return {
        isLoading: false,
          repos: action.payload || [],
      };
    default:
      return state
  }
}

export default spellReducer