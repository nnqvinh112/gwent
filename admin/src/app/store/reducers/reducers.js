import cardReducer from './card.reducer';
import {
  combineReducers
} from 'redux'
import factionReducer from './faction.reducer';
import spellReducer from './spell.reducer';

const rootReducers = combineReducers({
  cards: cardReducer,
  spells: spellReducer,
  factions: factionReducer,
})

export default rootReducers;