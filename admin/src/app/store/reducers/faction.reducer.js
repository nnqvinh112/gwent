import {
  LOAD_ALL_FACTIONS_ERROR,
  LOAD_ALL_FACTIONS_REQUEST,
  LOAD_ALL_FACTIONS_SUCCESS,
} from '../actions/faction.action'

const INITIAL_STATE = {
  isLoading: false,
  repos: []
}

function factionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_ALL_FACTIONS_REQUEST:
      return {
        isLoading: true,
      };
    case LOAD_ALL_FACTIONS_ERROR:
      return {
        isLoading: false,
          message: 'Failed to load factions',
          repos: [],
      };
    case LOAD_ALL_FACTIONS_SUCCESS:
      return {
        isLoading: false,
          repos: action.payload || [],
      };
    default:
      return state
  }
}

export default factionReducer