const loggerMiddleware = store => next => action => {
  const isFunction = typeof(action) === 'function';
  const actionType = isFunction ? typeof(action) : action.type;
  console.group(actionType)
  console.info('dispatching', action)
  let result = next(action)
  console.log('next state', store.getState())
  console.groupEnd()
  return result
}

export default loggerMiddleware