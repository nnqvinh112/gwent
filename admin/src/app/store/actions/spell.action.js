import SpellModel from "../../shared/models/spell.model";

/*
 * action types
 */

export const LOAD_ALL_SPELLS_REQUEST = 'LOAD_ALL_SPELLS_REQUEST'
export const LOAD_ALL_SPELLS_ERROR = 'LOAD_ALL_SPELLS_ERROR'
export const LOAD_ALL_SPELLS_SUCCESS = 'LOAD_ALL_SPELLS_SUCCESS'

/*
 * other constants
 */

const defaultSpells = [
  new SpellModel({
    id: 'S001',
    name: 'Resurrection',
    description: 'Bring 1 card back from the discarded pile',
    imageUrl: 'https://gamepedia.cursecdn.com/wowpedia/2/28/Spell_holy_holyprotection.png?version=ee1679e7770c351a1b3001c6432195da',
  }),
  new SpellModel({
    id: 'S002',
    name: 'Battle Cry',
    description: 'Double strength of all units in the same line',
    imageUrl: 'https://wow.zamimg.com/images/wow/icons/large/ability_warrior_battleshout.jpg',
  }),
  new SpellModel({
    id: 'S003',
    name: 'Spy',
    description: `Place on the enemy's table to withdraw 2 cards`,
    imageUrl: 'https://wow.zamimg.com/images/wow/icons/large/ability_stealth.jpg',
  }),
];

/*
 * action creators
 */

export function loadAllSpellsRequest() {
  return {
    type: LOAD_ALL_SPELLS_REQUEST
  }
}

export function loadAllSpellsError() {
  return {
    type: LOAD_ALL_SPELLS_ERROR
  }
}

export function loadAllSpellsSuccess() {
  return {
    type: LOAD_ALL_SPELLS_SUCCESS,
    payload: defaultSpells,
  }
}

export function loadAllSpellsAsync() {
  return (dispatch) => {
    dispatch(loadAllSpellsRequest());
    const delay = new Promise((resolve) => setTimeout(() => resolve(), 2000));
    delay.then(() => {
      dispatch(loadAllSpellsSuccess())
    })
  }
}
