import CardModel from '../../shared/models/card.model';

/*
 * action types
 */

export const LOAD_ALL_CARDS_REQUEST = 'LOAD_ALL_CARDS_REQUEST'
export const LOAD_ALL_CARDS_ERROR = 'LOAD_ALL_CARDS_ERROR'
export const LOAD_ALL_CARDS_SUCCESS = 'LOAD_ALL_CARDS_SUCCESS'
export const FILTER_CARDS = 'FILTER_CARDS'

/*
 * other constants
 */

const defaultHeroCard = new CardModel({
  name: 'Jaina Proudmoore',
  price: 1000,
  isHero: true,
  imageUrl: 'http://pre14.deviantart.net/b41a/th/pre/i/2014/318/8/a/jaina_proudmoore_hearthstone_hero_portrait_by_arsenal21-d86g4yd.jpg'
})

const defaultUnitCard = new CardModel({
  name: 'Footman',
  price: 250,
  imageUrl: 'https://i.pinimg.com/originals/ce/51/7b/ce517b96449aec43c2ed7e70528d9f01.jpg'
})

const defaultCards = [
  defaultHeroCard,
  defaultUnitCard,
  defaultUnitCard,
  defaultUnitCard,
];

/*
 * action creators
 */

export function loadAllCardsRequest() {
  return {
    type: LOAD_ALL_CARDS_REQUEST
  }
}

export function loadAllCardsError() {
  return {
    type: LOAD_ALL_CARDS_ERROR
  }
}

export function loadAllCardsSuccess() {
  return {
    type: LOAD_ALL_CARDS_SUCCESS,
    payload: defaultCards,
  }
}

export function loadAllCardsAsync() {
  return (dispatch) => {
    dispatch(loadAllCardsRequest());
    const delay = new Promise((resolve) => setTimeout(() => resolve(), 2000));
    delay.then(() => {
      dispatch(loadAllCardsSuccess())
    })
  }
}

export function filterCards(index) {
  return {
    type: FILTER_CARDS,
    index
  }
}