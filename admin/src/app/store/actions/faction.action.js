import FactionModel from "../../shared/models/faction.model";

/*
 * action types
 */

export const LOAD_ALL_FACTIONS_REQUEST = 'LOAD_ALL_FACTIONS_REQUEST'
export const LOAD_ALL_FACTIONS_ERROR = 'LOAD_ALL_FACTIONS_ERROR'
export const LOAD_ALL_FACTIONS_SUCCESS = 'LOAD_ALL_FACTIONS_SUCCESS'

/*
 * other constants
 */

const initialData = [
  new FactionModel({
    id: 'F001',
    name: 'The Alliance',
    description: `Humans, night elves, dwarves, gnomes, draenei, and the savage worgen make up the illustrious Alliance. Proud and noble, courageous and wise, these races work together to preserve order in Azeroth. The Alliance is driven by honor and tradition. Its rulers are champions of justice, hope, knowledge, and faith.`,
    imageUrl: 'https://gamepedia.cursecdn.com/wowpedia/thumb/d/da/Alliance_Crest.png/260px-Alliance_Crest.png?version=cd57dc6943e2213f8f0ac443934fc85b',
  }),
  new FactionModel({
    id: 'F002',
    name: 'The Horde',
    description: `The Horde is made up of orcs, forsaken, tauren, trolls, blood elves, and most recently, goblins. Misunderstood and cast aside, these diverse and powerful races strive to overcome their differences and unite as one in order to win freedom for their people and prosper in a land that has come to hate them. In the Horde, action and strength are valued above diplomacy, and its leaders earn respect by the blade, wasting no time with politics. The brutality of the Horde's champions is focused, giving a voice to those who fight for survival`,
    imageUrl: 'https://gamepedia.cursecdn.com/wowpedia/thumb/0/08/Horde_Crest.png/260px-Horde_Crest.png?version=fd8f1b2f97b57bdde74be1a30a584af2',
  }),
  new FactionModel({
    id: 'F003',
    name: 'The Scourge',
    description: `The Scourge (or Undead Scourge and Scourge Army) is one of three major undead factions existing in the world of Azeroth (the other two being the Forsaken and the Knights of the Ebon Blade). Until recently, it was a major player for control of the world. Created initially as the precursor to an invasion by the Burning Legion, the Scourge broke free from their demonic masters and — under the iron rule of the dread Lich King — have built up their power base on the Arctic continent of Northrend. Their influence spreads throughout Northrend, as well as the Plaguelands in northern Lordaeron and southern Quel'Thalas, and even Kalimdor to a small degree. Terrifying and insidious enemy, the Scourge, before its downfall, was perhaps the most dangerous threat to the world of Azeroth.`,
    imageUrl: 'https://gamepedia.cursecdn.com/wowpedia/thumb/9/94/Scourge_Crest.png/275px-Scourge_Crest.png?version=0e1e6120ef1a3cfdc7b3b77a43792e6f',
  }),
];

/*
 * action creators
 */

export function loadAllFactionsRequest() {
  return {
    type: LOAD_ALL_FACTIONS_REQUEST
  }
}

export function loadAllFactionsError() {
  return {
    type: LOAD_ALL_FACTIONS_ERROR
  }
}

export function loadAllFactionsSuccess() {
  return {
    type: LOAD_ALL_FACTIONS_SUCCESS,
    payload: initialData,
  }
}

export function loadAllFactionsAsync() {
  return (dispatch) => {
    dispatch(loadAllFactionsRequest());
    const delay = new Promise((resolve) => setTimeout(() => resolve(), 2000));
    delay.then(() => {
      dispatch(loadAllFactionsSuccess())
    })
  }
}