export const UnitTypes = {
  MELEE: {
    VALUE: 'MELEE',
    LABEL: 'Melee',
    ICON: '',
  },
  RANGED: {
    VALUE: 'RANGED',
    LABEL: 'Ranged',
    ICON: '',
  },
  MAGIC: {
    VALUE: 'MAGIC',
    LABEL: 'Magic',
    ICON: '',
  },
}

export const UnitTypesAsArray = [
  UnitTypes.MELEE,
  UnitTypes.RANGED,
  UnitTypes.MAGIC,
]