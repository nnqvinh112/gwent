class FactionModel {
  id = '';
  name = '';
  description = '';
  imageUrl = '';

  constructor(item) {
    if (item) {
      this.id = item.id;
      this.name = item.name || '';
      this.description = item.description || '';
      this.imageUrl = item.imageUrl || '';
    }
  }
}

export default FactionModel;