class CardModel {
  id = '';
  name = '';
  description = '';
  price = 0;
  isHero = false;
  imageUrl = '';
  damage = 0;
  unitType = '';

  constructor(item) {
    if (item) {
      this.id = item.id;
      this.name = item.name || '';
      this.description = item.description || '';
      this.price = item.price || 0;
      this.isHero = item.isHero || false;
      this.imageUrl = item.imageUrl || '';
      this.damage = item.damge || 0;
      this.unitType = item.unitType || '';
    }
  }
}

export default CardModel;