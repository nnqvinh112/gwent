class UnitTypeModel {
  id = '';
  name = '';
  imageUrl = '';

  constructor(item) {
    if (item) {
      this.id = item.id;
      this.name = item.name || '';
      this.imageUrl = item.imageUrl || '';
    }
  }
}

export default UnitTypeModel;