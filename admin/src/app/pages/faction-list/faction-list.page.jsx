import React, { Component } from 'react';

import CardDetails from '../../components/card-details/card-details';
import FactionCard from '../../components/faction-card/faction-card';
import TransitionableModal from '../../components/transitionable-modal/transitionable-modal';
import { connect } from 'react-redux';
import { loadAllFactionsAsync } from '../../store/actions/faction.action';

class FactionListPage extends Component {

  componentDidMount() {
    this.props.loadAllFactionsAsync();
  }

  render() {
    const { factions } = this.props;
    const addNewButton = (
      <button className="ui button blue">
        <i className="icon plus"></i>
        <span>Add Faction</span>
      </button>
    );
    return (
      <div className="container fluid">
        <h2 className="ui header dividing" style={{ display: 'flex' }}>
          <span>Faction</span>
          <div style={{ marginLeft: 'auto' }}></div>
          <div className="ui buttons icon mini labeled">
            <TransitionableModal
              trigger={addNewButton}
              action={(
                <button className="ui button blue">
                  <i className="icon save"></i>
                  Save
                  </button>
              )}>
              <CardDetails />
            </TransitionableModal>
          </div>
        </h2>
        <div className="ui items divided ">
          {
            (factions || []).map((faction, i) =>
              <TransitionableModal
                key={i}
                trigger={<FactionCard faction={faction} />}
                action={(
                  <button className="ui button blue">
                    <i className="icon save"></i>
                    Save
                  </button>
                )}>
                <CardDetails card={faction} />
              </TransitionableModal>
            )
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    factions: state.factions.repos
  }
}

const mapDispatchToProps = {
  loadAllFactionsAsync
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FactionListPage);