import React, { Component } from 'react';

import Card from '../../components/card/card';
import CardDetails from '../../components/card-details/card-details';
import TransitionableModal from '../../components/transitionable-modal/transitionable-modal';
import { connect } from 'react-redux';
import { loadAllCardsAsync } from '../../store/actions/card.action'

class CardListPage extends Component {

  componentDidMount() {
    this.props.loadAllCardsAsync();
  }

  render() {
    const { cards } = this.props;
    const addNewButton = (
      <button className="ui button blue">
        <i className="icon plus"></i>
        <span>Add Card</span>
      </button>
    );
    return (
      <div className="container fluid">
        <h2 className="ui header dividing" style={{ display: 'flex' }}>
          <span>Card</span>
          <div style={{ marginLeft: 'auto' }}></div>
          <div className="ui buttons icon mini labeled">
            <TransitionableModal
              trigger={addNewButton}
              action={(
                <button className="ui button blue">
                  <i className="icon save"></i>
                  Save
                  </button>
              )}>
              <CardDetails />
            </TransitionableModal>
          </div>
          <div className="ui buttons icon mini">
            <button className="ui button active">
              <i className="icon list"></i>
            </button>
            <button className="ui button">
              <i className="icon table"></i>
            </button>
          </div>
        </h2>
        <div className="ui grid">
          {
            (cards || []).map((card, i) =>
              <div className="two wide column" key={i}>
                <TransitionableModal
                  trigger={<Card card={card} />}
                  action={(
                    <button className="ui button blue">
                      <i className="icon save"></i>
                      Save
                  </button>
                  )}>
                  <CardDetails card={card} />
                </TransitionableModal>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cards: state.cards.repos
  }
}

const mapDispatchToProps = {
  loadAllCardsAsync
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardListPage);