import './ratio-image.css';

import React, { Component } from 'react'

export default class RatioImage extends Component {
  render() {
    return (
      <div className="image-background" style={{
        backgroundImage: `url(${this.props.src})`,
      }} />
    )
  }
}
