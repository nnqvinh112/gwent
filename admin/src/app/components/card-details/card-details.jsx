import React, { Component } from 'react'

import { Dropdown } from 'semantic-ui-react';
import RatioImage from '../ratio-image/ratio-image';
import UnitCardForm from '../unit-card-form/unit-card-form';
import { connect } from 'react-redux';
import { loadAllSpellsAsync } from '../../store/actions/spell.action';

export default class CardDetails extends Component {
  state = {
    isSpell: false,
  }

  render() {
    const { card } = this.props;
    const { isSpell } = this.state;

    return (
      <form className="ui grid">
        <div className="six wide column">
          <h3 className="ui dividing header">Image</h3>
          <div className="ui form">
            <div className="field">
              <RatioImage src={card.imageUrl} />
            </div>

            <div className="field">
              <div className="ui icon input">
                <input type="text" name="imageUrl" placeholder="Image's URL" />
                <i className="icon upload link"></i>
              </div>
            </div>
          </div>

        </div>
        <div className="ten wide column">
          <h3 className="ui dividing header">General Information</h3>
          <GeneralCardForm />

          <h3 className="ui dividing header">Combat Information
            <div className="ui toggle checkbox" style={{ float: 'right' }}>
              <input type="checkbox" name="isHero" checked={isSpell} onChange={() => this.setState({ isSpell: !isSpell })} />
              <label>Is Spell</label>
            </div>
          </h3>
          {isSpell ? <SpellCardForm /> : <UnitCardForm/>}
        </div>
      </form>
    )
  }
}


class GeneralCardForm extends Component {
  render() {

    const factions = [
      { value: "TA", text: 'The Alliance' },
      { value: "TH", text: 'The Horde' },
      { value: "TC", text: 'The Scourge' },
      { value: "TBL", text: 'The Burning Legion' },
    ]

    return (
      <div className="ui form">
        <div className="fields">
          <div className="six wide field">
            <label>Unique Id</label>
            <input type="text" name="id" readOnly />
          </div>

          <div className="ten wide field">
            <label>Name</label>
            <input type="text" name="name" />
          </div>
        </div>


        <div className="field">
          <label>Faction</label>
          <Dropdown
            options={factions}
            placeholder='Factions'
            clearable
            fluid multiple selection />
        </div>

        <div className="field">
          <label>Description</label>
          <textarea name="description" rows="3" />
        </div>

        <div className="fields">
          <div className="eight wide field">
            <label>Price</label>
            <input type="number" name="price" />
          </div>

          <div className="eight wide field">
            <label>Amount in shop</label>
            <input type="number" name="amount" />
          </div>
        </div>
      </div>
    )
  }
}

class SpellCardForm extends Component {
  render() {
    return (
      <div className="ui form">
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    spells: state.spells.repos
  }
}

const mapDispatchToProps = {
  loadAllSpellsAsync
}

connect(mapStateToProps, mapDispatchToProps)(GeneralCardForm);
connect(mapStateToProps, mapDispatchToProps)(SpellCardForm);