import React, { Component } from 'react'

export default class AppHeader extends Component {
  render() {
    return (
      <div className="ui menu huge inverted blue attached">
        <div className="header item">
          Gwent
        </div>
        {/* <a className="active item">Card</a>
        <a className="item">Photos</a> */}
      </div>
    )
  }
}
