import React, { Component } from 'react'

export default class SideMenu extends Component {
  render() {
    return (
      <div className="ui vertical fluid menu">
        <a className="item" href="cards">
          Card
      </a>
        <a className="item" href="factions">
          Factions
      </a>
        <a className="item" href="spells">
          Spells
      </a>
        <a className="item" href="players">
          Player
      </a>
        <a className="item" href="configs">
          Config
      </a>
      </div>
    )
  }
}
