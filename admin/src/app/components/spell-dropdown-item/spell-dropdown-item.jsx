import React, { Component } from 'react'

import { Header } from 'semantic-ui-react';

export default class SpellDropdownItem extends Component {
  render() {

    const { spell } = this.props;
    return (
      <Header image={spell.imageUrl} content={spell.name} subheader={spell.description} size="small" />
    )
  }
}
