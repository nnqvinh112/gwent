import { Modal, TransitionablePortal } from 'semantic-ui-react';
import React, { Component } from 'react'

export default class TransitionableModal extends Component {
  state = {
    open: false,
  }
  render() {
    const { trigger, children, closeIcon, size, centered, action } = this.props;
    const { open } = this.state;
    return (
      <TransitionablePortal open={open} trigger={trigger}>
        <Modal
          centered={centered || false}
          size={size || "small"}
          open={true}
          onClose={() => this.setState({ open: false })}
          closeIcon={closeIcon || true}>
          <Modal.Content>
            {children}
          </Modal.Content>
          {action && (<Modal.Actions>
            {action}
          </Modal.Actions>)}

        </Modal>
      </TransitionablePortal>
    )
  }
}
