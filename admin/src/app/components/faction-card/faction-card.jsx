import React, { Component } from 'react'

export default class FactionCard extends Component {
  render() {
    const { faction } = this.props;
    return (
      <div className="item">
        <div className="image">
          <img src={faction.imageUrl} alt='' />
        </div>
        <div className="content">
          <h4 className="header">{faction.name}</h4>
          <div className="meta">
            <span>Description</span>
          </div>
          <div className="description">
            <p>{faction.description}</p>
          </div>
          <div className="extra">
            <div className="ui right floated button mini compact">
              <i className="icon edit label"></i>
              <span>Edit</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
