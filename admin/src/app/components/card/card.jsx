import React, { Component } from 'react';

import RatioImage from '../ratio-image/ratio-image';

export default class Card extends Component {
  render() {
    const { card, onClick, active } = this.props;
    return (
      <div
        onClick={onClick}
        className={[
          'ui card fluid',
          (active || true) ? 'link' : '',
        ].join(' ')}
      >
        <div className="ui image card fluid">
          <RatioImage src={card.imageUrl} />
          {
            card.isHero &&
            <span className="ui green corner label mini">
              <i className="icon star"></i>
            </span>
          }
        </div>
        <div className="ui bottom attached label">
          <h5 className="header">{card.name}</h5>
        </div>
        {/* <div className="extra content">
          <span className="right floated">
            Joined in 2013
          </span>
          <span>
            <i className="user icon"></i>
            75 Friends
          </span>
        </div> */}
      </div>
    )
  }
}
