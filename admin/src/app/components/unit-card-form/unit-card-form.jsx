import React, { Component } from 'react'

import { Dropdown } from 'semantic-ui-react';
import SpellDropdownItem from '../spell-dropdown-item/spell-dropdown-item';
import { UnitTypesAsArray } from '../../shared/constants/card.constrant';
import { connect } from 'react-redux';
import { loadAllSpellsAsync } from '../../store/actions/spell.action';

class UnitCardForm extends Component {
  componentDidMount() {
    this.props.loadAllSpellsAsync();
  }

  render() {
    const unitTypes = UnitTypesAsArray.map(i => ({ value: i.VALUE, text: i.LABEL }));
    const spells = (this.props.spells || []).map(i =>
      ({
        value: i.id,
        text: i.name,
        content: <SpellDropdownItem spell={i} />,
      }),
    );

    return (
      <div className="ui form">
        <div className="fields">
          <div className="eight wide field">
            <label>Unit type</label>
            <Dropdown
              options={unitTypes}
              placeholder='Unit type'
              fluid selection />
          </div>

          <div className="eight wide field">
            <label>Damage</label>
            <input type="number" name="damage" />
          </div>
        </div>

        <div className="field">
          <label>Spell</label>
          <Dropdown
            options={spells}
            placeholder='Spells'
            fluid selection />
        </div>

        <div className="field">
          <label>Is Hero</label>
          <div className="ui labeled">
            <div className="ui toggle checkbox label fluid basic">
              <input type="checkbox" name="isHero" />
              <label>
                <h5 className="ui header blue float right">Hero cards are immune to spells</h5>
              </label>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    spells: state.spells.repos
  }
}

const mapDispatchToProps = {
  loadAllSpellsAsync
}

export default connect(mapStateToProps, mapDispatchToProps)(UnitCardForm);