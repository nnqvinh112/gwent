import React, { Component } from 'react'
import {
  Route,
  Switch
} from 'react-router-dom';

import CardListPage from '../../pages/card-list/card-list.page';
import FactionListPage from '../../pages/faction-list/faction-list.page';

export default class Router extends Component {
  render() {
    return (
      <Switch>
        <Route path="/cards">
          <CardListPage />
        </Route>
        <Route path="/factions">
          <FactionListPage />
        </Route>
        <Route path="/players">
        </Route>
        <Route path="/configs">
        </Route>
      </Switch>
    )
  }
}
