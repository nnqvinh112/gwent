import './App.css';

import AppHeader from './app/components/header/header';
import { BrowserRouter } from 'react-router-dom';
import React from 'react';
import Router from './app/components/router/router';
import SideMenu from './app/components/sidemenu/sidemenu';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <AppHeader />
        <div className="ui container fluid segment basic">
          <div className="ui grid">
            <div className="three wide column">
              <SideMenu />
            </div>
            <div className="ten wide column">
              <Router />
            </div>
            <div className="three wide column">
            </div>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
