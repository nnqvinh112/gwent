import { CardCollection } from '@app/shared/models/card-collection';
import { User } from '@app/shared/models/user';

export enum PlayerStatus {
  IN_LOBBY = 'IN_LOBBY',
  IN_MATCH = 'IN_MATCH',
  OFFLINE = 'OFFLINE',
  STAND_BY = 'STAND_BY'
}

export class Invitation {
  SentTo: string[] | object = [];
  ReceivedFrom: string[] | object = [];
}

export class Player extends User {
  Coin = 0;
  Collections: CardCollection[];
  CurrentMatch: string;
  Level = 1;
  MatchDrawed = 0;
  MatchLost = 0;
  MatchWon = 0;
  Status: PlayerStatus = PlayerStatus.STAND_BY;
  IsBot = false;

  constructor(data?: any) {
    super(data);
    if (data) {
      this.Coin = data.Coin || 0;
      this.Collections = (data.Collections || []).map((i: any) => new CardCollection(i));
      this.CurrentMatch = data.CurrentMatch;
      this.Level = data.Level || 1;
      this.MatchDrawed = data.MatchDrawed || 0;
      this.MatchLost = data.MatchLost || 0;
      this.MatchWon = data.MatchWon || 0;
      this.Status = data.Status || PlayerStatus.STAND_BY;
      this.IsBot = data.IsBot;
    }
  }
}
