export class CardCollection {
  Faction = '';
  CardsInDeck: string[] = [];
  CardsInCollection: string[] = [];

  constructor(data?: any) {
    if (data) {
      this.CardsInCollection = [...(data.CardsInCollection || [])];
      this.CardsInDeck = [...(data.CardsInDeck || [])];
      this.Faction = data.Faction || '';
    }
  }

  moveToDeck(cardId: string) {
    const indexInCollection = this.CardsInCollection.findIndex(id => id === cardId);
    if (indexInCollection >= 0) {
      const card = this.CardsInCollection.splice(indexInCollection, 1);
      this.CardsInDeck.push(...card);
    }
  }

  moveToCollection(cardId: string) {
    const indexInDeck = this.CardsInDeck.findIndex(id => id === cardId);
    if (indexInDeck >= 0) {
      const card = this.CardsInDeck.splice(indexInDeck, 1);
      this.CardsInCollection.push(...card);
    }
  }
}
