export class Card {
  Id = '';
  ImageUrl = '';
  IsHero = false;
  Name = '';
  SpellId = '';
  Damage = 0;
  UnitClass = '';

  constructor(data?: any) {
    if (data) {
      this.Id = data.Id;
      this.ImageUrl = data.ImageUrl;
      this.IsHero = data.IsHero;
      this.Name = data.Name;
      this.SpellId = data.SpellId;
      this.Damage = data.Damage;
      this.UnitClass = data.UnitClass;
    }
  }
}
