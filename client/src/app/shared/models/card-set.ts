import { Card } from './card';

export class CardSet {
  cards: Card[] = [];

  constructor(cards: Card[] = []) {
    this.cards = cards || [];
  }

  get unitCards(): Card[] {
    if (!this.cards) {
      return [];
    }
    return this.cards.filter(x => Boolean(x) && !x.IsHero);
  }

  get heroCards(): Card[] {
    if (!this.cards) {
      return [];
    }
    return this.cards.filter(x => Boolean(x) && x.IsHero);
  }
}
