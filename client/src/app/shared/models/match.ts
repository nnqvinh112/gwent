import { CardRow } from '@app/shared/models/card-row';
import { Player } from './player';
import { PlayerBoard } from './player-board';
import { Turn } from './turn';
import { UUID } from 'angular2-uuid';
import { User } from '@app/shared/models/user';

export enum MatchStatus {
  StandBy = 'STAND_BY',
  Playing = 'PLAYING',
  Player1Turn = 'PLAYER_1_TURN',
  Player2Turn = 'PLAYER_2_TURN',
  Player1Passed = 'PLAYER_1_PASSED',
  Player2Passed = 'PLAYER_2_PASSED',
  InitializingRound = 'INITIALIZING_ROUND',
  RoundEnded = 'ROUND_END',
  MatchEnded = 'MATCH_END',
  MatchInitialized = 'MATCH_INITIALIZED',
  SelectingDeck = 'SELECTING_DECK'
}

export class Match {
  Id = '';
  Status: MatchStatus;
  CreatedTime: Date;
  CurrentRound = 1;
  CurrentTurn: Turn;
  FirstTurn: Turn;

  PlayerBoards: PlayerBoard[];
  Players: User[];

  /** Result will store list of user id of who won each round */
  Result: string[] = [];

  static initializeNewMatch(player1: Player, player2: Player): Match {
    const match = new Match({
      Id: `${player1.Id}-${UUID.UUID()}`,
      Players: [player1, player2],
      Status: MatchStatus.SelectingDeck,
    });
    match.initializeBoardRows();
    return match;
  }

  /**
   * Initialize a match with inputs
   * @param id the match's id
   * @param players the players will take part in.
   * @param decks list of card id that will be used to generate drawed cards and cards in deck for both players
   */
  constructor(data?: any) {
    if (data) {
      this.Id = data.Id;
      this.Players = data.Players || [];
      this.Status = data.Status;
      this.CreatedTime = data.CreatedTime || new Date().toJSON();
      this.CurrentRound = data.CurrentRound || 1;
      this.CurrentTurn = data.CurrentTurn ? new Turn(data.CurrentTurn) : null;
      this.FirstTurn = data.FirstTurn ? new Turn(data.FirstTurn) : null;
      this.PlayerBoards = data.PlayerBoard || [];
      this.Players = data.Player || [];
      this.Result = data.Result || [];
    }
  }

  /**
   * Generate card row for both players.
   * Each player will have 1 row for each type of unit.
   * Each row will have 0 card.
   */
  private initializeBoardRows(): PlayerBoard[] {
    this.PlayerBoards = [new PlayerBoard(), new PlayerBoard()];
    return this.PlayerBoards;
  }
}
