import { Card } from '@app/shared/models/card';

export class CardRow {
  Cards: string[] | Card[] = [];
  Type = '';

  get length() {
    return this.Cards ? this.Cards.length : 0;
  }

  constructor(data?: any) {
    if (data) {
      this.Type = data.Type || data.type;
    }
  }
}
