import { CardRow } from './card-row';
import { GAMEPLAY } from '@app/shared/constants/gameplay.constant';
import { UNIT_CLASS } from '../constants/card.constant';

export class PlayerBoard {
  CardsInDeck: string[] = [];
  CardsOnBoard: CardRow[] = new Array(GAMEPLAY.NUM_ROW_ON_BOARD);
  DiscardedCards: string[] = [];
  DrawedCards: string[] = [];
  HasPassed = false;

  constructor(data?: any) {
    if (data) {
      this.CardsInDeck = data.CardsInDeck || [];
      this.CardsOnBoard = this.getInitialCardsOnBoard();
      if (data.CardsOnBoard) {
        this.CardsOnBoard = (data.CardsOnBoard || []).map((i: any) => new CardRow(i));
      }
      this.DiscardedCards = data.DiscardedCards || [];
      this.DrawedCards = data.DrawedCards || [];
      this.HasPassed = data.HasPassed || false;
    }
  }

  getInitialCardsOnBoard() {
    const typeKeys = Object.keys(UNIT_CLASS);
    const board: CardRow[] = [];
    typeKeys.forEach(key => {
      const row = new CardRow({ Type: UNIT_CLASS[key].NAME });
      board.push(row);
    });
    return board;
  }
}
