export class Turn {
  Time: Date;
  UserId = '';

  constructor(data?: Turn) {
    if (data) {
      this.Time = data.Time ? new Date(data.Time) : null;
      this.UserId = data.UserId;
    }
  }
}
