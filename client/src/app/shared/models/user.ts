export const PLAYER_TYPE = {
  BOT: 'BOT',
  USER: 'USER',
};

export class User {
  Email = '';
  Id = '';
  Name = '';
  ImageUrl = '';
  Type = PLAYER_TYPE.USER;

  constructor(data?: any) {
    if (data) {
      this.Id = data.Id;
      this.Name = data.Name || '';
      this.Email = data.Email || '';
      this.ImageUrl = data.ImageUrl || data.photoUrl || '';
    }
  }
}
