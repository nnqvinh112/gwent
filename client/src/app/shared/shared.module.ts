import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { CommonModule } from '@angular/common';
import { ComponentModule } from './components/component.module';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgModule } from '@angular/core';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '@env/environment';

@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    NgbTooltipModule,
    Ng2SearchPipeModule,
    FormsModule,
    ComponentModule
  ],
  declarations: [],
  exports: [
    NgbTooltipModule,
    AngularFireModule,
    AngularFireDatabaseModule,
    Ng2SearchPipeModule,
    FormsModule,
    ComponentModule
  ]
})
export class SharedModule {}
