export const ROUTE_SEGMENTS = {
  LOBBY: 'lobby',
  STORE: 'store',
  DECK: 'deck',
  DECK_PICKING: 'deck-picking',
  MATCH: 'match',
  HOME: 'home',
  LOGIN: 'login',
};
