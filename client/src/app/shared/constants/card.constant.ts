import { ICONS } from './asset.constant';

export const CARD_SIZES = {
  NORMAL: 'NORMAL',
  MEDIUM: 'MEDIUM',
  LARGE: 'LARGE'
};

export const CARD_POSITIONS = {
  ON_BOARD: 'ON_BOARD',
  ON_DECK: 'ON_DECK',
  DISCARDED: 'DISCARDED',
  DRAWED: 'DRAWED',
  ZOOMED: 'ZOOMED'
};

export const UNIT_CLASS = {
  MELEE: {
    NAME: 'MELEE',
    ICON: ICONS.MEELEE_UNIT
  },
  RANGED: {
    NAME: 'RANGED',
    ICON: ICONS.RANGED_UNIT
  },
  MAGIC: {
    NAME: 'MAGIC',
    ICON: ICONS.MAGIC_UNIT
  },
  MELEE_MAGIC: {
    NAME: 'MELEE_MAGIC',
    ICON: ICONS.MEELEE_UNIT
  }
};
