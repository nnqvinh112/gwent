export const ICONS = {
  ATTACK: 'assets/icons/dual-sword.svg',
  SWORD: 'assets/icons/sword.png',
  CARDS: 'assets/icons/cards.png',
  CARDS_STACK: 'assets/icons/card-stack.png',
  CARDS_DISCARDED: 'assets/icons/card-skull.png',
  HERO: 'assets/icons/helmet.svg',
  RANGED_UNIT: 'assets/icons/target.svg',
  MEELEE_UNIT: 'assets/icons/sword.svg',
  MAGIC_UNIT: 'assets/icons/magic.svg'
};

export const IMAGES = {
  CARD: {
    BACKGROUND: 'assets/images/card-back.png',
  }
};
