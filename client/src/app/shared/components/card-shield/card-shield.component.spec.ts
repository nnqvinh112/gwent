import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardShieldComponent } from './card-shield.component';

describe('CardShieldComponent', () => {
  let component: CardShieldComponent;
  let fixture: ComponentFixture<CardShieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardShieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardShieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
