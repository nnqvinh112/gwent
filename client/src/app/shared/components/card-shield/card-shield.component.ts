import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-shield',
  templateUrl: './card-shield.component.html',
  styleUrls: ['./card-shield.component.scss']
})
export class CardShieldComponent implements OnInit {
  @Input() set = 0;
  @Input() label = '';

  IMAGES = [
    {
      SHIELD: 'assets/images/elf-shield.png',
      SWORD: 'assets/images/elf-swordblade.png'
    },
    {
      SHIELD: 'assets/images/dark-icon-crest.png',
      SWORD: 'assets/images/keyblade-sword.png'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
