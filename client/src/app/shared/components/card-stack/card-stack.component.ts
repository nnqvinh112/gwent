import { Component, Input, OnInit } from '@angular/core';

import { CARD_TYPES } from '@app/shared/constants';
import { Card } from '@app/shared/models/card';

@Component({
  selector: 'app-card-stack',
  templateUrl: './card-stack.component.html',
  styleUrls: ['./card-stack.component.scss']
})
export class CardStackComponent implements OnInit {
  @Input()
  data: Card[] = Array();
  @Input()
  title: '';
  @Input()
  icon: '';
  @Input()
  isReveal = true;
  @Input()
  type = CARD_TYPES.NORMAL;

  get lastCard() {
    return this.data ? this.data[this.data.length - 1] : null;
  }
  get count() {
    return this.data ? this.data.length : 0;
  }
  constructor() {}

  ngOnInit() {}
}
