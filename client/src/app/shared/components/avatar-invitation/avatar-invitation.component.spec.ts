import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationAvatarComponent } from '@app/shared/components/avatar-invitation/avatar-invitation.component';

describe('AvatarInvitationComponent', () => {
  let component: InvitationAvatarComponent;
  let fixture: ComponentFixture<InvitationAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
