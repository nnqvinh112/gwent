import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Player, User } from '@app/shared/models';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-avatar-invitation',
  templateUrl: './avatar-invitation.component.html',
  styleUrls: ['./avatar-invitation.component.scss'],
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [
        style({ marginTop: 50, opacity: 0 }),
        animate(300, style({ marginTop: 0, opacity: 1 }))
      ])
    ])
  ],
})
export class InvitationAvatarComponent implements OnInit {
  @Input() data: User | Player;
  @Output() ok = new EventEmitter();
  @Output() cancel = new EventEmitter();

  get imageUrl() {
    return this.data ? this.data.ImageUrl : '';
  }
  get name() {
    return this.data ? this.data.Name : '';
  }
  constructor() { }

  ngOnInit() {
  }

  accept() {
    this.ok.emit(this.data);
  }
  decline() {
    this.cancel.emit(this.data);
  }
}
