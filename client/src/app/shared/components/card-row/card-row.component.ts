import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from '@angular/core';

import { Card } from '@app/shared/models';

interface RowData {
  card: Card;
  position: number;
}

@Component({
  selector: 'app-card-row',
  templateUrl: './card-row.component.html',
  styleUrls: ['./card-row.component.scss']
})
export class CardRowComponent implements OnInit, OnChanges {
  @Input()
  data: Card[] = [];
  @Input()
  isReveal = true;

  @Output()
  dblClickCard = new EventEmitter<Card>();

  @ViewChild('container', { static: false })
  containerRef: ElementRef;
  @ViewChildren('container')
  containerRefs: ElementRef[];

  displayedData: RowData[] = [];

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.computePosition(this.data);
  }

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      this.computePosition(changes.data.currentValue);
    }
  }

  computePosition(data: Card[]) {
    if (!data) {
      return;
    }

    const tmpArr: any[] = [];
    const cardSize = 70; // in pixel
    const containerSize = this.containerRef.nativeElement.clientWidth;
    const containerSpace = containerSize / data.length;
    const requiredSpace = cardSize;
    const allowedSpace = Math.min(containerSpace, requiredSpace);
    const center = containerSize * 0.5;
    data.forEach((element, index) => {
      tmpArr.push({
        card: element,
        position: center - allowedSpace * data.length * 0.5 + allowedSpace * (index + 0.5)
      });
    });
    this.displayedData = tmpArr;
  }

  onDblClick(row: RowData) {
    this.dblClickCard.emit(row.card);
  }
}
