import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';

import { Card } from '@app/shared/models';
import { ICONS } from '@app/shared/constants/asset.constant';
import { UNIT_CLASS } from '@app/shared/constants/card.constant';

@Component({
  selector: 'app-card-info',
  templateUrl: './card-info.component.html',
  styleUrls: ['./card-info.component.scss']
})
export class CardInfoComponent implements OnInit, OnChanges {
  @Input() data: Card;

  _ICON = {
    CLASS: this.getClassIcon(),
    ATTACK: ICONS.ATTACK,
    HERO: ICONS.HERO,
  };

  constructor() { }

  ngOnInit() {
    this._ICON.CLASS = this.getClassIcon();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      this._ICON.CLASS = this.getClassIcon();
    }
  }

  getClassIcon() {
    if (!this.data) {
      return '';
    }
    return UNIT_CLASS[this.data.UnitClass].ICON;
  }
}
