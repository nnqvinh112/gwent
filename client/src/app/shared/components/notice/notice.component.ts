import {
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
  animations: [
    trigger('slideDownAnimation', [
      transition(':leave', [
        style({ top: 0, opacity: 1 }),
        animate(200, style({ top: -100, opacity: 0 }))
      ]),
      transition(':enter', [
        style({ top: -100, opacity: 0 }),
        animate(200, style({ top: 0, opacity: 1 }))
      ])
    ])
  ],
})
export class NoticeComponent implements OnInit {
  @Input() message = '';
  @Input() show = false;
  @Input() showLoader = false;

  constructor() { }

  ngOnInit() { }
}
