import { BattlefieldRowComponent } from './battlefield-row/battlefield-row.component';
import { BoardAvatarComponent } from './avatar/avatar-board.component';
import { CardComponent } from './card/card.component';
import { CardInfoComponent } from './card-info/card-info.component';
import { CardRowComponent } from './card-row/card-row.component';
import { CardShieldComponent } from './card-shield/card-shield.component';
import { CardStackComponent } from './card-stack/card-stack.component';
import { CommonModule } from '@angular/common';
import { DeckCollectionComponent } from './deck-collection/deck-collection.component';
import { DeckSummaryComponent } from './deck-summary/deck-summary.component';
import { InvitationAvatarComponent } from './avatar-invitation/avatar-invitation.component';
import { LoaderComponent } from './loader/loader.component';
import { LobbyAvatarComponent } from './avatar-lobby/avatar-lobby.component';
import { NgModule } from '@angular/core';
import { NoticeComponent } from './notice/notice.component';
import { StatBoxComponent } from './stat-box/stat-box.component';

@NgModule({
  imports: [CommonModule],
  exports: [
    BoardAvatarComponent,
    CardComponent,
    CardInfoComponent,
    BattlefieldRowComponent,
    CardStackComponent,
    CardRowComponent,
    InvitationAvatarComponent,
    LoaderComponent,
    LobbyAvatarComponent,
    NoticeComponent,
    StatBoxComponent,
    CardShieldComponent,
    DeckCollectionComponent,
    DeckSummaryComponent
  ],
  declarations: [
    BoardAvatarComponent,
    CardComponent,
    CardInfoComponent,
    BattlefieldRowComponent,
    CardStackComponent,
    CardRowComponent,
    InvitationAvatarComponent,
    LoaderComponent,
    LobbyAvatarComponent,
    NoticeComponent,
    StatBoxComponent,
    CardShieldComponent,
    DeckCollectionComponent,
    DeckSummaryComponent
  ],
  providers: []
})
export class ComponentModule {}
