import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stat-box',
  templateUrl: './stat-box.component.html',
  styleUrls: ['./stat-box.component.scss']
})
export class StatBoxComponent implements OnInit {
  @Input() data = '';
  @Input() icon = '';
  @Input() fontSize = 14;
  @Input() isColumn = false;

  constructor() { }

  ngOnInit() {
  }

}
