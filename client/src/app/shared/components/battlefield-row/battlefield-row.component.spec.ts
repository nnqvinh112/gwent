import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BattlefieldRowComponent } from '@app/shared/components/battlefield-row/battlefield-row.component';

describe('CardRowComponent', () => {
  let component: BattlefieldRowComponent;
  let fixture: ComponentFixture<BattlefieldRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BattlefieldRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BattlefieldRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
