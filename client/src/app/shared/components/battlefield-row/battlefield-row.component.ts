import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import { ICON, UNIT_CLASS } from '@app/shared/constants';

import { CARD_TYPES } from '@app/shared/constants';
import { Card } from '@app/shared/models/card';

@Component({
  selector: 'app-battlefield-row',
  templateUrl: './battlefield-row.component.html',
  styleUrls: ['./battlefield-row.component.scss']
})
export class BattlefieldRowComponent implements OnInit, OnChanges {
  @Input()
  data: Card[] = Array();
  @Input()
  type = 'undefined';
  @Output()
  damageChange = new EventEmitter<number>();
  _CARD_TYPE = CARD_TYPES;
  _ICON = { DAMAGE: ICON.ATTACK };

  damage = 0;

  constructor() {}

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.updateRow(changes.data);
    this.updateType();
    this.onDamageChange();
  }
  updateRow(data: SimpleChange) {
    if (!data || !data.currentValue) {
      return;
    }
    const cards = data.currentValue
      .filter((x: Card) => this.type.toUpperCase() === x.UnitClass.toUpperCase())
      .sort((left: Card, right: Card) => this.sort(left, right));

    this.damage = 0;
    cards.forEach((card: Card) => (this.damage += card.Damage));
  }

  updateType() {
    this.type = UNIT_CLASS[this.type.toUpperCase()].NAME.toLowerCase();
  }

  sort(left: Card, right: Card) {
    if (left.Damage > right.Damage) {
      return 1;
    }
    if (left.Damage < right.Damage) {
      return -1;
    }
    if (left.Name > right.Name) {
      return 1;
    }
    if (left.Name < right.Name) {
      return -1;
    }
    return 0;
  }
  onDamageChange() {
    this.damageChange.emit(this.damage);
  }
}
