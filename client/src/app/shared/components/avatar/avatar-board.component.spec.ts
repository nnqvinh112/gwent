import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardAvatarComponent } from '@app/shared/components/avatar/avatar-board.component';

describe('AvatarComponent', () => {
  let component: BoardAvatarComponent;
  let fixture: ComponentFixture<BoardAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
