import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { GAMEPLAY } from '@app/shared/constants';
import { ICON } from '@app/shared/constants';
import { Player } from '@app/shared/models/player';

@Component({
  selector: 'app-avatar-board',
  templateUrl: './avatar-board.component.html',
  styleUrls: ['./avatar-board.component.scss']
})
export class BoardAvatarComponent implements OnInit, OnChanges {
  @Input()
  damage = 0;
  @Input()
  data = new Player();
  @Input()
  hasPassed = false;
  @Input()
  isReversed = false;
  @Input()
  result = this.initResult();

  _ICON = {
    CARDS: ICON.CARDS,
    DAMAGE: ICON.ATTACK
  };
  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.result) {
      this.result = (this.result || this.initResult())
        .filter(x => x !== this.data.Id)
        .concat(this.initResult())
        .splice(0, GAMEPLAY.NUM_ROUND_TO_WIN);
    }
  }
  initResult(): string[] {
    return Array(GAMEPLAY.NUM_ROUND_TO_WIN).map(() => {
      return this.data.Id;
    });
  }
  isLifeDisabled(userId: string) {
    if (!this.data) {
      return false;
    }
    return userId && userId !== this.data.Id;
  }
}
