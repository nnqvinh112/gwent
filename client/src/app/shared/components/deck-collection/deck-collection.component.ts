import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { Card } from './../../models/card';
import { KeyValue } from './../../models/key-value';

@Component({
  selector: 'app-deck-collection',
  templateUrl: './deck-collection.component.html',
  styleUrls: ['./deck-collection.component.scss']
})
export class DeckCollectionComponent implements OnInit, OnChanges {
  @Input() label = '';
  @Input() cardSet: Card[] = [];
  @Input() hints: KeyValue[] = [];

  @Output() cardClick = new EventEmitter<Card>();
  @Output() cardDblClick = new EventEmitter<Card>();
  @Output() cardHover = new EventEmitter<Card>();
  @Output() cardBlur = new EventEmitter<Card>();

  constructor() {}

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.hints && !(changes.hints instanceof Array)) {
      this.hints = Object.keys(this.hints).map(key => ({ key, value: this.hints[key] } as KeyValue));
    }
  }

  onHover(card: Card) {
    this.cardHover.next(card);
  }

  onBlur(card: Card) {
    this.cardBlur.next(card);
  }

  onClick(card: Card) {
    this.cardClick.next(card);
  }

  onDblClick(card: Card) {
    this.cardDblClick.next(card);
  }
}
