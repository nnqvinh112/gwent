import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeckCollectionComponent } from './deck-collection.component';

describe('DeckCollectionComponent', () => {
  let component: DeckCollectionComponent;
  let fixture: ComponentFixture<DeckCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeckCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeckCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
