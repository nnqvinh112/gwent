import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { Card } from '@app/shared/models/card';
import { CardSet } from '@app/shared/models/card-set';
import { GAMEPLAY } from '@app/shared/constants/gameplay.constant';

@Component({
  selector: 'app-deck-summary',
  templateUrl: './deck-summary.component.html',
  styleUrls: ['./deck-summary.component.scss']
})
export class DeckSummaryComponent implements OnInit, OnChanges {
  @Input() label = '';
  @Input() cardSet: Card[] | CardSet;
  @Input() canValidate = true;

  get isValid() {
    return !this.canValidate || this._cardSet.cards.length >= this._minDeckSize;
  }

  _minDeckSize = GAMEPLAY.NUM_CARD_PER_MATCH;
  _cardSet: CardSet = new CardSet();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'cardSet') {
        const cards = changes[propName].currentValue;
        if (cards instanceof Array) {
          return (this._cardSet = new CardSet(cards));
        }
        return (this._cardSet = cards);
      }
    }
  }
}
