import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Player, User, PLAYER_TYPE } from '@app/shared/models';

@Component({
  selector: 'app-avatar-lobby',
  templateUrl: './avatar-lobby.component.html',
  styleUrls: ['./avatar-lobby.component.scss']
})
export class LobbyAvatarComponent implements OnInit {
  @Input() data: User | Player;
  @Input() playable = true;
  @Input() isSelf = false;
  @Output() invite = new EventEmitter();

  get imageUrl() {
    return this.data ? this.data.ImageUrl : '';
  }
  get name() {
    return this.data ? this.data.Name : '';
  }
  get level() {
    return this.data ? this.data['Level'] || '???' : '???';
  }
  get isFriendable() {
    return !this.isSelf && this.data.Type !== PLAYER_TYPE.BOT;
  }
  constructor() { }

  ngOnInit() {
  }

  onInvite() {
    this.invite.emit(this.data);
  }
}
