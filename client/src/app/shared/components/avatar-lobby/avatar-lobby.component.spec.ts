import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyAvatarComponent } from '@app/shared/components/avatar-lobby/avatar-lobby.component';

describe('AvatarLobbyComponent', () => {
  let component: LobbyAvatarComponent;
  let fixture: ComponentFixture<LobbyAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
