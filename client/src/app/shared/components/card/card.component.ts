import { CARD_SIZES, UNIT_CLASS } from '@app/shared/constants/card.constant';
import { Component, Input, OnInit } from '@angular/core';

import { Card } from '@app/shared/models/card';
import { IMAGES } from '@app/shared/constants/asset.constant';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() config = { type: CARD_SIZES.NORMAL, isReveal: true };
  @Input() data: Card;
  @Input() type = CARD_SIZES.NORMAL;
  @Input() isReveal = true;
  @Input() isHoverable = true;

  CARD_SIZES = CARD_SIZES;

  get isShowable() {
    return this.data && this.isReveal;
  }

  get unitClassSvg() {
    if (this.data && this.data.UnitClass) {
      return UNIT_CLASS[this.data.UnitClass] ? UNIT_CLASS[this.data.UnitClass].ICON : '';
    }
    return;
  }

  get backgroundImage() {
    return this.isShowable && this.data.ImageUrl ? this.data.ImageUrl : IMAGES.CARD.BACKGROUND;
  }

  get wrapperCss() {
    return {
      'card__wrapper--active': this.isShowable
    };
  }

  get cardCss() {
    return {
      'card--large': this.type === CARD_SIZES.LARGE,
      'uk-transition-slide-bottom-small': this.isHoverable && this.isShowable
    };
  }
  get flagCss() {
    return this.isShowable ? (this.data.IsHero ? 'card__flag--hero' : 'card__flag--unit') : null;
  }
  constructor() {}

  ngOnInit() {}
}
