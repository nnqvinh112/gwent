// Contain app config
// ===========================================================================================
export const APP = {
  VERSION: '1.0.1-dev-27/09/18',
};

// Define icons used for entire app
// ===========================================================================================
export const ICON = {
  ATTACK: 'assets/icons/dual-sword.png',
  SWORD: 'assets/icons/sword.png',
  CARDS: 'assets/icons/cards.png',
  CARDS_STACK: 'assets/icons/card-stack.png',
  CARDS_DISCARDED: 'assets/icons/card-skull.png',
  RANGED_UNIT: 'assets/icons/arrow.png',
  MEELEE_UNIT: 'assets/icons/sword.png',
  MAGIC_UNIT: 'assets/icons/magic.png',
};

// Define class for unit card
// ===========================================================================================
export const UNIT_CLASS = {
  MELEE: {
    NAME: 'MELEE',
    ICON: ICON.MEELEE_UNIT,
  },
  RANGED: {
    NAME: 'RANGED',
    ICON: ICON.RANGED_UNIT,
  },
  MAGIC: {
    NAME: 'MAGIC',
    ICON: ICON.MAGIC_UNIT,
  },
  MELEE_MAGIC: {
    NAME: 'MELEE_MAGIC',
    ICON: ICON.MEELEE_UNIT,
  },
};

export const CARD_TYPES = {
  NORMAL: 'NORMAL',
  MEDIUM: 'MEDIUM',
  LARGE: 'LARGE',
  ON_BOARD: 'ON_BOARD',
  ON_DECK: 'ON_DECK',
  DISCARDED: 'DISCARDED',
  DRAWED: 'DRAWED',
  ZOOMED: 'ZOOMED',
};

export const GAMEPLAY = {
  NUM_ROW_ON_BOARD: 3,
  NUM_ROUND_TO_WIN: 2,
  NUM_CARD_FIRST_DRAW: 10,
  NUM_CARD_PER_MATCH: 22,
  TIME_SCREEN_MESSAGE: 1500,
  TIME_FOR_EACH_TURN: 30000,
};

export const SOCIAL_LOGIN = {
  PROVIDER: {
    GOOGLE: '347745475248-ikl8mng90ojricdge14p2st2iq2klvsf.apps.googleusercontent.com',
    FACEBOOK: '',
  }
};

// Route use accross the app
// ===========================================================================================
export const ROUTE = {
  MATCH: {
    ROOT: 'match',
  },
  DECK: {
    ROOT: 'deck',
  },
};
