import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { I18nService, Logger } from '@app/core';
import { LoadAllBots, LoadLoggedUser } from './core/actions/user.action';
import { combineLatest, forkJoin, merge } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

import { APP } from '@app/shared';
import { LoadAllCards } from './core/actions/card.action';
import { Store } from '@ngxs/store';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';

const log = new Logger('App');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  version: string = APP.VERSION;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private i18nService: I18nService,
    private store: Store
  ) {}

  ngOnInit() {
    this.preloadData();
    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    log.debug('init');

    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    const onNavigationEnd = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        mergeMap(route => route.data)
      )
      .subscribe(event => {
        const title = event['title'];
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
  }

  preloadData() {
    forkJoin([
      this.store.dispatch(LoadAllCards),
      this.store.dispatch(LoadLoggedUser),
      this.store.dispatch(LoadAllBots)
    ]).subscribe(data => {
      console.log('TCL: AppComponent -> preloadData -> data', data);
    });
  }
}
