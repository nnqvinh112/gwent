import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialLoginModule
} from 'angularx-social-login';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CardState } from './core/states/card.state';
import { CoreModule } from '@app/core';
import { HttpClientModule } from '@angular/common/http';
import { MatchState } from './core/states/match.state';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { SOCIAL_LOGIN } from '@app/shared/constants';
import { SharedModule } from '@app/shared';
import { TranslateModule } from '@ngx-translate/core';
import { UserState } from './core/states/user.state';
import { environment } from '@env/environment';

// Configs
export function getAuthServiceConfigs() {
  return new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(SOCIAL_LOGIN.PROVIDER.FACEBOOK)
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(SOCIAL_LOGIN.PROVIDER.GOOGLE)
    }
  ]);
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    TranslateModule.forRoot(),
    NgxsModule.forRoot([CardState, UserState, MatchState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    CoreModule,
    SharedModule,
    SocialLoginModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AppRoutingModule // AppRoutingModule must be below any other routing module
  ],
  declarations: [AppComponent],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
