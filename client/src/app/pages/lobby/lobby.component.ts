import { Component, OnDestroy, OnInit } from '@angular/core';
import { Invitation, PLAYER_TYPE, Player, PlayerStatus } from '@app/shared/models';
import { Subscription, combineLatest } from 'rxjs';

import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { CardState } from '@app/core/states/card.state';
import { LobbyService } from '@app/core/provider/lobby.service';
import { MatchService } from '@app/core/provider/match.service';
import { NoticeService } from '@app/core/provider/notice.service';
import { ROUTE_SEGMENTS } from './../../shared/constants/route.constant';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UserService } from '@app/core/provider/user.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {
  isLoading = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private matchService: MatchService,
    private noticeService: NoticeService,
    private userService: UserService,
    private lobbyService: LobbyService,
    private store: Store
  ) {
    // this.isLoading = true;
    // this.noticeService.show('Loading', { showLoader: true });
    // // 1. Set the player's status to IN_LOBBY
    // this.userService.setStatus(this.playerId, PLAYER_STATUS.IN_LOBBY);
    // this.subscriber.add(
    //   combineLatest([
    //     // 2. Load current player in lobby
    //     this.getCurrentPlayer(),
    //     // 2. Load all player in lobby
    //     this.getLobbyPlayers()
    //   ])
    //     .pipe(
    //       switchMap(res => {
    //         // 3. Load all invitation
    //         return this.getInvitation(this.playerId);
    //       })
    //     )
    //     .subscribe(res => {
    //       this.isLoading = false;
    //       this.noticeService.hide();
    //       if (this.currentPlayer.CurrentMatch) {
    //         this.getMatch(this.currentPlayer.CurrentMatch);
    //       }
    //     })
    // );
  }

  ngOnInit() {}

  // getCurrentPlayer() {
  //   return this.userService.getUserById(this.playerId).pipe(map(player => (this.currentPlayer = <Player>player)));
  // }

  // getLobbyPlayers() {
  //   return this.userService.getLobbyPlayers().pipe(
  //     map(res => {
  //       return (this.players = res);
  //     })
  //   );
  // }

  // /**
  //  * Load all invitations the player sent/received.
  //  * @param playerId
  //  */
  // getInvitation(playerId: string) {
  //   return this.userService.getInvitation(playerId).pipe(
  //     map(res => {
  //       if (!res) {
  //         return (this.invitation = null);
  //       }
  //       // Convert id list of string to player type
  //       const converted = new Invitation();
  //       Object.keys(res).forEach(key => {
  //         res[key].forEach((id: string) => (<Player[]>converted[key]).push(this.getUserById(id)));
  //       });
  //       return (this.invitation = converted);
  //     })
  //   );
  // }
  // getUserById(userId: string) {
  //   return this.players ? this.players.find(x => x.Id === userId) : null;
  // }

  /**
   * Load match with the given Id.
   * If the match exists, check for its status, otherwise do nothing
   * If the match's status = SELECTING_FACTION, then navigate to deck page,
   * otherwise, navigate to board page.
   */
  // getMatch(id: string) {
  //   this.subscriber.add(
  //     this.matchService.getMatchById(id).subscribe(
  //       match => {
  //         if (match) {
  //           if (match.Status === MATCH_STATUS.SELECTING_FACTION) {
  //             this.router.navigate([`/${ROUTE.DECK.ROOT}/${this.currentPlayer.CurrentMatch}`]);
  //           } else {
  //             this.router.navigate([`/${ROUTE.MATCH.ROOT}/${this.currentPlayer.CurrentMatch}`]);
  //           }
  //         }
  //       },
  //       error => console.error(error)
  //     )
  //   );
  // }

  /** Call match initializing method from service */
  // initializeMatch(opponent: Player) {
  //   return this.matchService.createMatch(opponent as Player, this.authService.user as Player).then(response => {
  //     if (response) {
  //       this.noticeService.hide();
  //     }
  //   });
  // }
  // invitePlayer(user: Player) {
  //   if (user.Type === PLAYER_TYPE.BOT) {
  //     this.initializeMatch(user).then(() => this.matchService.assignDeck(user.Id, user.Collections[0].CardsInDeck));
  //   } else {
  //     this.userService.sendInvitation(this.authService.user, user);
  //   }
  // }

  /*
   * Check whether the invitation hasn't expired.
   * If it is still available, then initialize a match.
   */
  // invitationAccepted(user: Player) {
  //   // TO DO: check invitation's expiration
  //   console.log(user);
  //   this.initializeMatch(user);
  // }

  onPvPClick() {}

  onPvEClick() {
    this.lobbyService.joinPvE().then(match => {
      this.router.navigate(['/', ROUTE_SEGMENTS.MATCH, ROUTE_SEGMENTS.DECK_PICKING, match.Id]);
    });
  }

  ngOnDestroy() {}
}
