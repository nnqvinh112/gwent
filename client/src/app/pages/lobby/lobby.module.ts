import { CommonModule } from '@angular/common';
import { LobbyComponent } from './lobby.component';
import { LobbyRoutingModule } from './lobby-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    LobbyRoutingModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    LobbyComponent,
  ],
})
export class LobbyModule { }
