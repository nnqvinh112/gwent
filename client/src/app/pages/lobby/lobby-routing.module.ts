import { RouterModule, Routes } from '@angular/router';

import { LobbyComponent } from './lobby.component';
import { NgModule } from '@angular/core';

const routes: Routes = [{ path: '', component: LobbyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LobbyRoutingModule {}
