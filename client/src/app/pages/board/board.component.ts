import { UNIT_CLASS } from '@shared/constants/card.constant';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { Card } from '@app/shared/models/card';
import { GAMEPLAY, ICON } from '@app/shared/constants';
import { CardRow } from '@app/shared/models/card-row';
import { CardService } from '@app/core/provider/card.service';
import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, HostListener } from '@angular/core';
import { Match, MatchStatus } from '@app/shared/models/match';
import { MatchService } from '@app/core/provider/match.service';
import { NoticeService } from '@app/core/provider/notice.service';
import { PLAYER_TYPE, PlayerStatus } from '@app/shared/models';
import { UserService } from '@app/core/provider/user.service';
import { switchMap, map } from 'rxjs/operators';

const INTERVAL = 300;
const CARD_ROWS = [UNIT_CLASS.MELEE, UNIT_CLASS.RANGED, UNIT_CLASS.MAGIC];
const MATCH_RESULT = {
  PLAYER_WON: 0,
  OPPONENT_WON: 1,
  DRAWN: 2,
  UNKNOWN: -1
};

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnDestroy {
  _CARD_ROWS = CARD_ROWS;
  _ICON = {
    DECK: ICON.CARDS_STACK,
    DISCARDED: ICON.CARDS_DISCARDED,
    CARDS: ICON.CARDS
  };

  hasSubmitted = false;
  hasPassed = [false, false];
  isBotMode = false;
  isLoading = true;

  selectedCard: Card;
  cards: Card[] = new Array();
  cardsDiscarded: Card[][] = [Array(), Array()];
  cardsDrawed: Card[][] = [Array(), Array()];
  cardsInDecks: Card[][] = [Array(), Array()];
  cardsPlaced: CardRow[][] = [Array(), Array()];
  totalDamage = [0, 0];

  gameTimer: any;
  turnTimer = GAMEPLAY.TIME_FOR_EACH_TURN;

  lastMessage: string[] = [];
  match: Match;
  pIndex = [0, 0]; // stored player actual index in data. This maybe different from local index.
  subscriber = new Subscription();

  @HostListener('document:keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    if (event.key === 'Escape' || event.key === 'Esc') {
      this.finishMatch(1);
    }
  }

  get currentPlayer() {
    return this.authService.user;
  }
  get maxRound() {
    return GAMEPLAY.NUM_ROUND_TO_WIN + 1;
  }
  get isPlayerTurn() {
    return true;
    // return this.match.CurrentTurn === this.currentPlayer.Id;
  }
  get remainingTime() {
    return (this.turnTimer / GAMEPLAY.TIME_FOR_EACH_TURN) * 100;
  }

  constructor(
    private authService: AuthenticationService,
    private cardService: CardService,
    private userService: UserService,
    private matchService: MatchService,
    private noticeService: NoticeService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.isLoading = true;
    // this.noticeService.show('Loading', true);
    // Loading Data
    this.subscriber.add(
      // 1. Get all available cards
      this.getAllCard()
        // 2. Load the current match
        .pipe(switchMap(() => this.getMatch()))
        .subscribe(() => {
          this.convertAllCard();
          this.computeDamage();
          this.isLoading = false;
          // FOR BOT ONLY. TURN ON BOT'S SCRIPTS IF AVAILABLE
          if (this.match.Players[this.pIndex[1]].Type === PLAYER_TYPE.BOT && !this.isBotMode) {
            this.isBotMode = true;
          }
        })
    );
    this.subscriber.add(this.checkForWinner().subscribe());
    this.subscriber.add(this.cardService.hoveredCard.subscribe((card: Card) => (this.selectedCard = card)));
    this.userService.setStatus(this.currentPlayer.Id, PlayerStatus.IN_MATCH);
    this.interval();
  }

  /**
   * Validate the pIndex to make sure it's value is between 0 and 1.
   */
  validateIndex(index?: number) {
    if (index !== undefined) {
      return index >= 0 && index < 2;
    }
    for (let i = 0; i < this.pIndex.length; i += 1) {
      if (this.pIndex[i] < 0 || this.pIndex[i] >= 2) {
        throw new Error(`Player indexes are invalid: ${this.pIndex}`);
      }
    }
  }
  /**
   * Load the current match from server.
   */
  getMatch(): Observable<Match> {
    return this.route.params.pipe(
      switchMap(params => {
        return this.matchService.getMatchById(params.id).pipe(
          map(res => {
            this.pIndex[0] = res.Players.findIndex(key => key.Id === this.currentPlayer.Id);
            this.pIndex[1] = 1 - this.pIndex[0];
            this.match = res;
            this.validateIndex();
            this.showNotice();
            return res;
          })
        );
      })
    );
  }
  /**
   * Check and show the notice relevant to the current event
   */
  showNotice() {
    // const index = this.matchService.getPlayerIndex(this.match.CurrentTurn);
    // const message: string[] = [];
    // const option = { timeout: GAMEPLAY.TIME_SCREEN_MESSAGE };
    // switch (this.match.Status) {
    //   case MATCH_STATUS.ROUND_FINISHED:
    //     const roundWinner = this.hasRoundWinner();
    //     if (roundWinner === MATCH_RESULT.DRAWN) {
    //       message.push(`Round ${this.match.CurrentRound} end in a draw.`);
    //     } else {
    //       message.push(`${!roundWinner ? 'You' : 'Opponent'} won round ${this.match.CurrentRound}`);
    //     }
    //     break;
    //   case MATCH_STATUS.MATCH_FINISHED:
    //     const matchWinner = this.hasMatchWinner();
    //     if (matchWinner === MATCH_RESULT.DRAWN) {
    //       return this.noticeService.show(`DRAW`, option);
    //     } else {
    //       return this.noticeService.show(!matchWinner ? 'VICTORY' : 'LOST', option);
    //     }
    //   case MATCH_STATUS.MATCH_INITIALIZED:
    //     message.push(`${index === this.pIndex[0] ? 'You' : 'Opponent'} play first`);
    //     break;
    //   case MATCH_STATUS.INITIALIZE_ROUND:
    //     message.push(`Round ${this.match.CurrentRound}`);
    //     message.push(`${index === this.pIndex[0] ? `You` : `Opponent`} play first`);
    //     break;
    //   case MATCH_STATUS.PLAYER_1_PASSED:
    //     message.push(`${index === this.pIndex[0] ? 'You' : `Opponent`} has passed`);
    //     break;
    //   case MATCH_STATUS.PLAYER_2_PASSED:
    //     message.push(`${index === this.pIndex[0] ? 'You' : `Opponent`} has passed`);
    //     break;
    //   case MATCH_STATUS.PLAYER_1_TURN:
    //     message.push(`${index === this.pIndex[0] ? 'Your' : `Opponent's`} turn`);
    //     break;
    //   case MATCH_STATUS.PLAYER_2_TURN:
    //     message.push(`${index === this.pIndex[0] ? 'Your' : `Opponent's`} turn`);
    //     break;
    // }
    // if (!this.lastMessage.some(x => message.includes(x))) {
    //   this.noticeService.show(message, option);
    //   this.lastMessage = message;
    // }
  }
  /**
   * Convert all card id from board, deck, drawed pile into card type
   */
  convertAllCard() {
    this.convertCardsOnBoard(); // Cards on board functional differently from the others
    // this.cardsDrawed = this.convertCards(this.match.DrawedCards);
    // this.cardsInDecks = this.convertCards(this.match.CardsInDeck);
    // this.cardsDiscarded = this.convertCards(this.match.Discardedcards);
  }
  private convertCards(cardIdList: string[][]): Card[][] {
    const cards: Card[][] = [[], []];
    for (let i = 0; i < 2; i += 1) {
      const pIndex = this.pIndex[i];
      if (!cardIdList || !cardIdList[pIndex]) {
        cards[pIndex] = [];
        continue;
      }
      // cards[pIndex] = this.cardService.convertIdsToCards(cardIdList[pIndex]);
    }
    return cards;
  }
  /**
   * Get all cards available on server
   */
  getAllCard(): Observable<Card[]> {
    return this.cardService.getAllCards().pipe(map(res => (this.cards = res)));
  }
  /**
   * Convert cards on board for both players. Each player has 3 rows of on-board cards
   */
  convertCardsOnBoard() {
    // if (!this.match.CardsOnBoard) {
    //   return (this.cardsPlaced = [new Array(), new Array()]);
    // }
    // this.match.CardsOnBoard.forEach((pCards, index) => {
    //   if (!pCards) {
    //     return;
    //   }
    //   pCards.forEach((row, rowIndex) => {
    //     this.cardsPlaced[index][rowIndex] = <CardRow>{
    //       Type: row.Type,
    //       Cards: row.Cards ? this.cardService.getCardsByIds(row.Cards as string[]) : []
    //     };
    //   });
    // });
  }
  /**
   * Return the length of the given array
   */
  arrayLength(arr: any[]) {
    return arr ? arr.length : 0;
  }
  submitCard(card: Card, index: number) {
    if (this.isSubmittable(index)) {
      const pIndex = this.pIndex[index];
      this.hasSubmitted = true;
      this.turnTimer = GAMEPLAY.TIME_FOR_EACH_TURN;
      this.matchService.submitCard(pIndex, card).then(() => {
        // Check for passing round if there is no card left
        if (this.arrayLength(this.cardsDrawed[pIndex]) <= 0) {
          return this.pass(index).then(() => this.finishSubmit());
        }
        // If opponent's has passed, then continue the player turn
        // if (this.match.HasPassed[+!pIndex]) {
        //   return this.finishSubmit();
        // }
        // this.switchTurn(index).then(() => this.finishSubmit());
      });
    }
  }
  /**
   * Turn hasSubmitted off to allow further submision
   */
  finishSubmit() {
    this.hasSubmitted = false;
  }
  /**
   * Check if the player hasn't passed and takes the current turn
   */
  isSubmittable(index: number) {
    return !this.hasPlayerPassed(index) && this.isPlayerTurn === !index && !this.hasSubmitted;
  }
  /**
   * Pass the round and finish the round if both players have passed, otherwise switch turn
   */
  pass(index: number) {
    return Promise.resolve();
    // return this.matchService.pass(this.pIndex[index], this.match.HasPassed).then(() => {
      // if (!this.match.HasPassed[0] || !this.match.HasPassed[1]) {
      //   this.switchTurn(index);
      // } else {
      //   this.finishRound();
      // }
    // });
  }
  /**
   * Switch turn between players
   */
  switchTurn(index: number) {
    return this.matchService.switchTurn(this.pIndex[index]);
  }
  /**
   * Decide the round's winner and check for finish the match if possible
   */
  finishRound() {
    const index = this.hasRoundWinner();
    if (index === MATCH_RESULT.DRAWN) {
      return this.matchService.finishRound();
    }
    if (this.validateIndex(index)) {
      const winnderId = this.match.Players[this.pIndex[index]].Id;
      return this.matchService.finishRound(winnderId);
    }
    return Promise.resolve();
  }
  /**
   * Finish the match with/without a winner
   * @param index index of the winner
   */
  finishMatch(index?: number) {
    this.ngOnDestroy();
    this.matchService.finishMatch(this.match, this.pIndex[index]).then(() => {
      setTimeout(() => {
        this.noticeService.clear();
        this.noticeService.hide();
        this.router.navigate(['/lobby'], { replaceUrl: true });
      }, GAMEPLAY.TIME_SCREEN_MESSAGE);
    });
  }
  startNewRound() {
    return this.matchService.startNewRound();
  }
  /**
   * Check whether the player has passed the round
   */
  hasPlayerPassed(index: number) {
    return false;
    // return this.match.HasPassed && this.match.HasPassed[this.pIndex[index]];
  }
  /**
   * Check if there are a winner of the match.
   * @return result code
   */
  hasMatchWinner() {
    if (!this.match.Result) {
      return MATCH_RESULT.UNKNOWN;
    }
    const lostRound = (index: number) => {
      return this.match.Result.filter(x => x === this.match.Players[this.pIndex[index]].Id);
    };
    if (lostRound(0).length >= GAMEPLAY.NUM_ROUND_TO_WIN && lostRound(1).length >= GAMEPLAY.NUM_ROUND_TO_WIN) {
      return MATCH_RESULT.DRAWN;
    }
    if (lostRound(0).length >= GAMEPLAY.NUM_ROUND_TO_WIN) {
      return MATCH_RESULT.PLAYER_WON;
    }
    if (lostRound(1).length >= GAMEPLAY.NUM_ROUND_TO_WIN) {
      return MATCH_RESULT.OPPONENT_WON;
    }
    return MATCH_RESULT.UNKNOWN;
  }
  /**
   * Check if there are a winner of the current round.
   * @return result code
   */
  hasRoundWinner(): number {
    if (!this.hasPlayerPassed(0) || !this.hasPlayerPassed(1)) {
      return MATCH_RESULT.UNKNOWN;
    }
    if (this.totalDamage[this.pIndex[0]] > this.totalDamage[this.pIndex[1]]) {
      return MATCH_RESULT.PLAYER_WON;
    }
    if (this.totalDamage[this.pIndex[0]] < this.totalDamage[this.pIndex[1]]) {
      return MATCH_RESULT.OPPONENT_WON;
    }
    return MATCH_RESULT.DRAWN;
  }
  /**
   * Check if there is a winner of a round/match
   */
  checkForWinner() {
    return this.noticeService.closed.pipe(
      map(() => {
        const matchWinner = this.hasMatchWinner();
        const roundWinner = this.hasRoundWinner();
        console.log(matchWinner, roundWinner, this.match.Status);

        if (this.match.Status === MatchStatus.MatchEnded) {
          this.finishMatch(matchWinner);
        } else if (this.match.Status === MatchStatus.RoundEnded) {
          if (matchWinner >= 0) {
            this.matchService.updateStatus(MatchStatus.MatchEnded);
          } else if (roundWinner >= 0) {
            this.startNewRound();
          }
        }
      })
    );
  }
  /**
   * Compute total damage of all rows for each player
   * @returns {number[]} array of damage of each player
   */
  computeDamage() {
    if (!this.totalDamage) {
      this.totalDamage = [0, 0];
    }
    // For each player
    for (let i = 0; i < 2; i += 1) {
      const pIndex = this.pIndex[i];
      let totalDamage = 0;
      // For each row
      if (!this.cardsPlaced[pIndex]) {
        throw new Error(`Could not compute row damage for null: ${this.cardsPlaced[pIndex]}`);
      }
      this.cardsPlaced[pIndex].forEach((row, rowIndex) => {
        // For each card, add card's damage to total damage
        (<Card[]>row.Cards).forEach(card => (totalDamage += card.Damage || 0));
      });
      this.totalDamage[pIndex] = totalDamage || 0;
    }
    return this.totalDamage;
  }
  /**
   * Sort card by damage, then by name
   */
  sort(cards: Card[]) {
    if (!cards) {
      return new Array();
    }
    return cards.sort((left, right) => {
      if (left.Damage > right.Damage) {
        return 1;
      }
      if (left.Damage < right.Damage) {
        return -1;
      }
      if (left.Name > right.Name) {
        return 1;
      }
      if (left.Name < right.Name) {
        return -1;
      }
      return 0;
    });
  }
  interval() {
    this.gameTimer = setInterval(() => {
      this.turnTimer -= INTERVAL;
      if (
        this.isBotMode &&
        !this.isPlayerTurn &&
        !this.hasSubmitted &&
        this.isSubmittable(1) &&
        !this.noticeService.isShowing &&
        this.noticeService.remainingMessage.length === 0
      ) {
        this.botTakeTurn();
      }
    }, INTERVAL);
  }
  ngOnDestroy(): void {
    clearInterval(this.gameTimer); // Turn off auto-bot
    this.subscriber.unsubscribe();
  }
  /**
   * BOT AUTOPLAY METHODS.
   * THESE METHODS SHOULD BE TURNED ON ONLY IF THE OPPONENT IS BOT
   */
  botTakeTurn() {
    const pIndex = this.pIndex[1];
    // Pass if there is no card left or the player's passed with lower damage
    if (
      !this.cardsDrawed[pIndex] ||
      this.cardsDrawed[pIndex].length <= 0 ||
      (this.totalDamage[pIndex] > this.totalDamage[this.pIndex[0]] && this.hasPlayerPassed(0))
    ) {
      this.pass(1);
    } else if (!this.isPlayerTurn && this.cardsDrawed && this.cardsDrawed[pIndex]) {
      const card = this.cardsDrawed[pIndex][0];
      this.submitCard(card, 1);
    }
  }
}
