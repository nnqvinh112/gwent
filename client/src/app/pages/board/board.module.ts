import { BoardComponent } from './board.component';
import { BoardRoutingModule } from './board-routing.module';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@app/core';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    BoardRoutingModule,
  ],
  exports: [],
  declarations: [BoardComponent],
  providers: [],
})
export class BoardModule { }
