import { Route, extract } from '@app/core';
import { RouterModule, Routes } from '@angular/router';

import { BoardComponent } from './board.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: 'match/:id', component: BoardComponent, data: { title: extract('Playing') } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class BoardRoutingModule { }
