import { Player } from '@app/shared/models';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { UserService } from '@app/core/provider/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLoading: boolean;
  user: Player;

  get appLogo() {
    return '../../assets/images/app-logo.png';
  }
  constructor(private authService: AuthenticationService, private userService: UserService) {}

  ngOnInit() {
    this.isLoading = true;
    this.userService.getPlayerById(this.authService.user.Id).subscribe(user => {
      this.user = <Player>user;
      this.isLoading = false;
    });
  }
}
