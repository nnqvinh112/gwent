import { CommonModule } from '@angular/common';
import { DeckComponent } from './deck.component';
import { DeckRoutingModule } from './deck-routing.mudule';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    DeckRoutingModule,
  ],
  exports: [],
  declarations: [DeckComponent],
  providers: [],
})
export class DeckModule { }
