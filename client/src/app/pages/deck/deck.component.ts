import * as _ from 'lodash';

import { ActivatedRoute, Router } from '@angular/router';
import { Card, Match, MatchStatus, Player } from '@app/shared/models';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IMatchState, MatchState } from './../../core/states/match.state';
import { IUserState, UserState } from '@app/core/states/user.state';
import { Observable, Subscription, of } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { AuthenticationService } from '@app/core';
import { CardService } from '@app/core/provider/card.service';
import { CardState } from '@app/core/states/card.state';
import { ConverterService } from './../../core/provider/converter.service';
import { DeckService } from './../../core/provider/deck.service';
import { LoadMatchById } from '@app/core/actions/match.action';
import { MatchService } from '@app/core/provider/match.service';
import { ROUTE_SEGMENTS } from '@app/shared/constants/route.constant';
import { UserService } from '@app/core/provider/user.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss']
})
export class DeckComponent implements OnInit, OnDestroy {
  @Select(CardState) cardState$: Observable<CardState>;

  selectedDeckIndex = 0;
  selectedCard: Card;

  get player() {
    return this.store.selectSnapshot<IUserState>(UserState).currentUser;
  }

  get match() {
    return this.store.selectSnapshot<IMatchState>(MatchState).match;
  }

  get factionName(): string {
    if (!this.player) {
      return '';
    }
    const collections = this.player.Collections;
    if (!collections || !collections[this.selectedDeckIndex]) {
      return '';
    }
    return this.player.Collections[this.selectedDeckIndex].Faction;
  }

  get cardsInDeck(): Card[] {
    const deck = _.get(this.player, ['Collections', this.selectedDeckIndex, 'CardsInDeck']);
    const cards = this.converter.convertIdsToCards(deck || []);
    return _.sortBy(cards, ['Damage', 'Name']);
  }

  get cardsInCollection(): Card[] {
    const deck = _.get(this.player, ['Collections', this.selectedDeckIndex, 'CardsInCollection']);
    const cards = this.converter.convertIdsToCards(deck || []);
    return _.sortBy(cards, ['Damage', 'Name']);
  }

  /** The player is ready when he has assigned his deck */
  get isReady() {
    return true;
    // if (!this.player) {
    //   return false;
    // }
    // const pIndex = this.matchService.getPlayerIndex(this.player.Id);
    // if (!this.match.CardsInDeck || !this.match.CardsInDeck[pIndex]) {
    //   return false;
    // }
    // return this.match.CardsInDeck[pIndex].length >= this.minCardInDeck;
  }

  /**
   * Check if both players have assigned their deck
   */
  get areAllPlayerReady() {
    // if (
    //   !this.match.CardsInDeck ||
    //   this.match.CardsInDeck.length < 2 ||
    //   !this.match.CardsInDeck[0].length ||
    //   !this.match.CardsInDeck[1].length
    // ) {
    //   return false;
    // }
    return true;
  }

  constructor(
    private authService: AuthenticationService,
    private cardService: CardService,
    private deckService: DeckService,
    private matchService: MatchService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store,
    private converter: ConverterService
  ) {}

  ngOnInit() {
    // Load match id from route params.
    // If the id is exit, then load the match matched the id.
    const params = this.route.snapshot.params;
    if (params && params.id) {
      this.loadMatch(params.id);
    }
  }

  ngOnDestroy() {}

  loadMatch(id: string) {
    this.store.dispatch(new LoadMatchById(id)).subscribe();
    // this.matchService.getMatchById(id).subscribe((res: Match) => {
    //   this.match = res;
    //   this.checkForStartingMatch();
    // });
  }

  /**
   * Check if both players have assigned their deck,
   * if true, then update match status and navigate to board page
   */
  checkForStartingMatch() {
    // if (this.areAllPlayerReady) {
    //   if (this.match.Status === MatchStatus.SelectingDeck) {
    //     this.matchService.updateStatus(MatchStatus.MatchInitialized).then(() => this.goToMatch(this.match.Id));
    //   } else {
    //     this.goToMatch(this.match.Id);
    //   }
    // }
  }

  goToMatch(id: string) {
    this.router.navigate(['/', ROUTE_SEGMENTS.MATCH, id], { replaceUrl: true });
  }
  /**
   * Navigate between factions
   */
  nextFaction() {
    if (!this.player || !this.player.Collections) {
      return;
    }
    this.selectedDeckIndex = (this.selectedDeckIndex + 1) % this.player.Collections.length;
  }
  previousFaction() {
    if (!this.player || !this.player.Collections) {
      return;
    }
    const length = this.player.Collections.length;
    this.selectedDeckIndex = (length + this.selectedDeckIndex - 1) % length;
  }

  selectCard(card?: Card) {
    this.selectedCard = card;
  }

  /**
   * Assign the current deck to the match
   */
  ready() {
    // this.matchService.assignDeck(this.player.Id, this.cardsInDeck.map(res => res.Id));
  }

  moveCardToCollection(card: Card) {
    this.deckService.moveCardToStock(this.selectedDeckIndex, card.Id);
  }

  moveCardToDeck(card: Card) {
    this.deckService.moveCardToDeck(this.selectedDeckIndex, card.Id);
  }
}
