import { Route, extract } from '@app/core';
import { RouterModule, Routes } from '@angular/router';

import { DeckComponent } from './deck.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DeckComponent,
    data: { title: extract('Deck') }
  },
  {
    path: ':id',
    component: DeckComponent,
    data: { title: extract('Deck Selection') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class DeckRoutingModule {}
