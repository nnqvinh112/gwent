import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { extract } from '@app/core';

const routes: Routes = [
  { path: '', component: LoginComponent, data: { title: extract('Login') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LoginRoutingModule { }
