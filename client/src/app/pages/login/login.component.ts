import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { AuthenticationService, I18nService, Logger } from '@app/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize, switchMap } from 'rxjs/operators';

import { ROUTE_SEGMENTS } from '@app/shared/constants/route.constant';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

const _LOG = new Logger('Login');
const _SOCIAL_KEY = {
  FACEBOOK: 'Facebook',
  GOOGLE: 'Google'
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  _SOCIAL_KEY = _SOCIAL_KEY;
  error: string;
  loginForm: FormGroup;
  isLoading = false;

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private i18nService: I18nService,
    private socialAuthService: AuthService,
    private authService: AuthenticationService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  ngOnDestroy(): void {}

  login() {
    this.isLoading = true;
    this.authService
      .login(this.loginForm.value)
      .pipe(
        finalize(() => {
          this.loginForm.markAsPristine();
          this.isLoading = false;
        })
      )
      .subscribe(
        user => {
          _LOG.debug(`${user.Name} successfully logged in`);
          this.navigate();
        },
        error => {
          _LOG.debug(`Login error: ${error}`);
          this.error = error;
        }
      );
  }

  socialLogIn(socialPlatform: string) {
    const socialPlatformProvider = this.getProvider(socialPlatform);
    this.isLoading = true;
    from(this.socialAuthService.signIn(socialPlatformProvider))
      .pipe(
        untilDestroyed(this),
        switchMap(userData => this.authService.loginWithSocialAccount(userData)),
        finalize(() => (this.isLoading = false))
      )
      .subscribe(
        userData => {
          _LOG.debug(`${userData.Name} successfully logged in`);
          this.navigate();
        },
        error => _LOG.debug(`Social login error: ${error}`)
      );
  }

  getProvider(socialPlatform: string) {
    switch (socialPlatform) {
      case _SOCIAL_KEY.FACEBOOK:
        return FacebookLoginProvider.PROVIDER_ID;
      case _SOCIAL_KEY.GOOGLE:
        return GoogleLoginProvider.PROVIDER_ID;
      default:
        return null;
    }
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  navigate() {
    this.router.navigate(['/', ROUTE_SEGMENTS.LOBBY], { replaceUrl: true });
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: [{ value: '', disabled: true }, Validators.required],
      password: [{ value: '', disabled: true }, Validators.required],
      remember: true
    });
  }
}
