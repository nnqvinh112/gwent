import { Route, extract } from '@app/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  Route.withShell([
    { path: 'about', component: AboutComponent, data: { title: extract('About') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AboutRoutingModule { }
