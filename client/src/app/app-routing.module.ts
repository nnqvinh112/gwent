import { Route, extract } from '@app/core';
import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { ROUTE_SEGMENTS } from './shared/constants/route.constant';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: `/${ROUTE_SEGMENTS.LOBBY}` },
  {
    path: ROUTE_SEGMENTS.LOGIN,
    loadChildren: () => import('@pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: ROUTE_SEGMENTS.MATCH,
    children: [
      {
        path: ROUTE_SEGMENTS.DECK_PICKING,
        loadChildren: () => import('@pages/deck/deck.module').then(m => m.DeckModule)
      }
    ]
  },
  Route.withShell([
    {
      path: ROUTE_SEGMENTS.HOME,
      data: { title: extract('Home') },
      loadChildren: () => import('@pages/home/home.module').then(m => m.HomeModule)
    },
    {
      path: ROUTE_SEGMENTS.DECK,
      data: { title: extract('Deck') },
      loadChildren: () => import('@pages/deck/deck.module').then(m => m.DeckModule)
    },
    {
      path: ROUTE_SEGMENTS.LOBBY,
      data: { title: extract('Lobby') },
      loadChildren: () => import('@pages/lobby/lobby.module').then(m => m.LobbyModule)
    }
  ]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
