import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { I18nService } from '@app/core/i18n.service';
import { ICONS } from '@shared/constants/asset.constant';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  ICONS =  {
    BATTLE: ICONS.ATTACK,
  };

  menuHidden = true;

  get appLogo() {
    return '../../../../assets/images/app-logo.png';
  }
  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private i18nService: I18nService) { }

  ngOnInit() { }

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    this.authenticationService.logout()
      .subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get name(): string | null {
    const user = this.authenticationService.user;
    return user ? user.Name : null;
  }

  get image(): string | null {
    const user = this.authenticationService.user;
    return user ? user.ImageUrl : null;
  }
}
