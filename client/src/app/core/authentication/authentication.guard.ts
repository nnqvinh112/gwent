import { CanActivate, Router } from '@angular/router';

import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Injectable } from '@angular/core';
import { Logger } from '@app/core/logger.service';
import { ROUTE_SEGMENTS } from '@app/shared/constants/route.constant';

const log = new Logger('AuthenticationGuard');

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}

  canActivate(): boolean {
    if (this.authenticationService.isAuthenticated()) {
      return true;
    }

    log.debug('Not authenticated, redirecting...');
    this.router.navigate(['/', ROUTE_SEGMENTS.LOGIN], { replaceUrl: true });
    return false;
  }
}
