import * as _ from 'lodash';

import { BehaviorSubject, Observable, from, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { FirebaseService } from '@app/core/provider/firebase.service';
import { FirestoreService } from './../provider/firestore.service';
import { Injectable } from '@angular/core';
import { Player } from '@app/shared/models';
import { SocialUser } from 'angularx-social-login';
import { User } from '@app/shared/models/user';
import { UserService } from '@app/core/provider/user.service';
import { tap } from 'rxjs/internal/operators/tap';

export interface LoginContext {
  username: string;
  password: string;
  email: string;
  remember?: boolean;
}

const KEYS = {
  CONFIG: 'config',
  DEFAULT_PROFILE: 'default.account'
};
const LOCAL_KEY = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {
  private _user$ = new BehaviorSubject<User>(null);

  /**
   * Gets the user credentials.
   * @return {User} The user credentials or null if the user is not authenticated.
   */
  get user(): User {
    return this._user$.value;
  }

  constructor(
    private firebase: FirebaseService,
    private userService: UserService,
    private firestore: FirestoreService
  ) {
    const savedCredentials = sessionStorage.getItem(LOCAL_KEY) || localStorage.getItem(LOCAL_KEY);
    if (savedCredentials) {
      this._user$.next(JSON.parse(savedCredentials));
    }
  }

  /**
   * Authenticates the user.
   * @param {User} context The login parameters.
   * @param {Boolean} remember Is able to store data in local
   * @return {Observable<User>} The user credentials.
   */
  login(context: LoginContext): Observable<User> {
    // Replace by proper authentication call
    const tmpUser = new User({
      Id: context.username,
      Name: context.username,
      Email: context.email
    });
    this.setUser(tmpUser, context.remember);
    return of(tmpUser);
  }

  /**
   * Log in with social account
   */
  loginWithSocialAccount(user: SocialUser, remember?: boolean): Observable<User> {
    return this.userService.getPlayerById(user.id).pipe(
      switchMap(res => (res ? of(res) : this.getInitialAccountData(user))),
      switchMap((res: User) => {
        res.Email = user.email;
        res.Name = user.name;
        res.ImageUrl = user.photoUrl;
        res.Id = user.id;
        return from(this.userService.updateUser(res)).pipe(
          map(() => {
            this.setUser(res, remember);
            return res;
          })
        );
      })
    );
  }
  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.setUser();
    return of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return Boolean(this.user);
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {User =} user The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  private setUser(user?: User, remember?: boolean) {
    this._user$.next(user);

    if (user) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(LOCAL_KEY, JSON.stringify(user));
    } else {
      sessionStorage.removeItem(LOCAL_KEY);
      localStorage.removeItem(LOCAL_KEY);
    }
  }

  updateUser(user: User): Promise<void> {
    if (!user.Id) {
      throw new Error(`Could not update user with no Id.`);
    }
    return this.firebase.update(`players/${user.Id}`, user);
  }

  signUp(user: User) {}

  private getInitialAccountData(user: SocialUser): Observable<Player> {
    return this.firestore.get(KEYS.CONFIG).pipe(
      first(),
      map(res => {
        if (res && res.length) {
          const config = res.shift();
          const defaultAccount = _.get(config, KEYS.DEFAULT_PROFILE);
          const account = new Player(defaultAccount);
          return account;
        }
        throw Error(`Missing config or the ${KEYS.DEFAULT_PROFILE} key in config`);
      })
    );
  }
}
