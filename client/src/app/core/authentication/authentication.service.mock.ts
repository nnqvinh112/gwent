import { LoginContext } from '@app/core/authentication/authentication.service';
import { Observable, of } from 'rxjs';
import { User } from '@app/shared/models/user';

export class MockAuthenticationService {

  credentials: User | null = {
    Id: '123',
    ImageUrl: 'sample image',
    Name: 'test',
    Email: '',
    Type: '',
  };

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

}
