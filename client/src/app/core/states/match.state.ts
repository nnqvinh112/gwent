import { Action, Selector, State, StateContext } from '@ngxs/store';

import { LoadMatchById } from '../actions/match.action';
import { Match } from '@app/shared/models';
import { MatchService } from '../provider/match.service';
import { tap } from 'rxjs/internal/operators/tap';

export interface IMatchState {
  match: Match;
}

@State<IMatchState>({
  name: 'matches',
  defaults: {
    match: null
  }
})
export class MatchState {
  constructor(private matchService: MatchService) {}

  @Action(LoadMatchById)
  loadMatchById(ctx: StateContext<IMatchState>, action: LoadMatchById) {
    return this.matchService.getMatchById(action.id).pipe(
      tap(match => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          match
        });
      })
    );
  }
}
