import { Action, Selector, State, StateContext } from '@ngxs/store';
import { LoadAllBots, LoadLoggedUser } from '../actions/user.action';
import { Player, User } from '@app/shared/models';

import { AuthenticationService } from '../authentication/authentication.service';
import { CardCollection } from '@app/shared/models/card-collection';
import { UserService } from '../provider/user.service';
import { of } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';

export interface IUserState {
  currentUser: Player;
  allBots: Player[];
}

@State<IUserState>({
  name: 'users',
  defaults: {
    currentUser: null,
    allBots: []
  }
})
export class UserState {
  @Selector()
  static currentUser(state: IUserState) {
    return state.currentUser;
  }

  @Selector()
  static cardCollections(state: IUserState): CardCollection[] {
    return state.currentUser.Collections || [];
  }

  constructor(private userService: UserService, private authService: AuthenticationService) {}

  @Action(LoadLoggedUser)
  loadLoggedUser(ctx: StateContext<IUserState>) {
    if (!this.authService.isAuthenticated()) {
      return of(null);
    }
    const loggedUser = this.authService.user;
    return this.userService.getPlayerById(loggedUser.Id).pipe(
      tap(user => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentUser: user
        });
      })
    );
  }

  @Action(LoadAllBots)
  loadAllBots(ctx: StateContext<IUserState>) {
    return this.userService.getAllBots().pipe(
      tap(bots => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          allBots: bots
        });
      })
    );
  }
}
