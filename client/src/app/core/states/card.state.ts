import { Action, Selector, State, StateContext } from '@ngxs/store';

import { Card } from '@app/shared/models';
import { CardService } from '@app/core/provider/card.service';
import { LoadAllCards } from '../actions/card.action';
import { tap } from 'rxjs/internal/operators/tap';

export interface ICardState {
  allCards: Card[];
}

@State<ICardState>({
  name: 'cards',
  defaults: {
    allCards: []
  }
})
export class CardState {
  constructor(private cardService: CardService) {}

  @Action(LoadAllCards)
  loadAllCards(ctx: StateContext<ICardState>) {
    return this.cardService.getAllCards().pipe(
      tap(cards => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          allCards: cards
        });
      })
    );
  }
}
