export class LoadMatchById {
  static readonly type = '[Match] Load Match By Id';
  constructor(public id: string) {}
}
