export class LoadLoggedUser {
  static readonly type = '[Player] Load Logged User';
  constructor() {}
}

export class LoadAllBots {
  static readonly type = '[Player] Load All Bots';
  constructor() {}
}
