import { Match, Player } from '@app/shared/models';
import { Observable, of } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { FirestoreService } from './firestore.service';
import { IUserState } from './../states/user.state';
import { Injectable } from '@angular/core';
import { MatchService } from './match.service';
import { UserService } from './user.service';
import { UserState } from '../states/user.state';

@Injectable()
export class LobbyService {
  @Select(UserState.currentUser) currentUser$: Observable<Player>;

  get userStore() {
    return this.store.selectSnapshot<IUserState>(UserState);
  }

  constructor(
    private firestore: FirestoreService,
    private userService: UserService,
    private store: Store,
    private matchService: MatchService
  ) {}

  joinLobby() {
    // this.userService.setStatus();
  }

  joinPvE(): Promise<Match> {
    const bots = this.userStore.allBots;
    const randomInt = Math.floor(Math.random() * bots.length);
    const opponent = bots[randomInt];
    return this.matchService.createMatch(this.userStore.currentUser, opponent);
  }

  joinPvP() {}
}
