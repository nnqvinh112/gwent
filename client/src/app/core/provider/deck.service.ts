import * as _ from 'lodash';

import { CardCollection } from '@app/shared/models/card-collection';
import { FirestoreService } from './firestore.service';
import { IUserState } from '../states/user.state';
import { Injectable } from '@angular/core';
import { Player } from '@app/shared/models';
import { Store } from '@ngxs/store';
import { UserService } from './user.service';
import { UserState } from './../states/user.state';

const DATABASE_PATH = {
  PLAYERS: 'players'
};

@Injectable()
export class DeckService {
  constructor(private firestore: FirestoreService, private store: Store, private userService: UserService) {}

  moveCardToStock(collectionIndex: number, cardId: string) {
    const player = new Player(this.store.selectSnapshot<IUserState>(UserState).currentUser);
    const collection = _.get(player, ['Collections', collectionIndex]);
    collection.moveToCollection(cardId);
    player.Collections[collectionIndex] = collection;
    return this.firestore.update(DATABASE_PATH.PLAYERS, player.Id, player);
  }

  moveCardToDeck(collectionIndex: number, cardId: string) {
    const player = new Player(this.store.selectSnapshot<IUserState>(UserState).currentUser);
    const collection = _.get(player, ['Collections', collectionIndex]);
    collection.moveToDeck(cardId);
    player.Collections[collectionIndex] = collection;
    return this.firestore.update(DATABASE_PATH.PLAYERS, player.Id, player);
  }
}
