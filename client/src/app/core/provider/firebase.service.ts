import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class FirebaseService {

  constructor(private firebase: AngularFireDatabase) { }

  get(path: string, query?: { orderByChild: string, equalTo: string }) {
    if (query) {
      return this.firebase.list(
        path,
        ref => ref
          .orderByChild(query.orderByChild)
          .equalTo(query.equalTo)
      );
    }
    return this.firebase.list(path);
  }
  push(path: string, data: any) {
    return this.firebase.list(path).push(JSON.parse(JSON.stringify(data)));
  }
  set(path: string, data: any) {
    return this.firebase.object(path).set(JSON.parse(JSON.stringify(data)));
  }
  update(path: string, data: any) {
    return this.firebase.object(path).update(JSON.parse(JSON.stringify(data)));
  }
}
