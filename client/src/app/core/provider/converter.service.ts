import { CardState, ICardState } from '../states/card.state';

import { Card } from '@app/shared/models';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';

@Injectable()
export class ConverterService {

  constructor(private store: Store) { }

  convertIdsToCards(idList: string[]): Card[] {
    if (!idList) {
      return [];
    }
    const allCards = this.store.selectSnapshot<ICardState>(CardState).allCards;
    return idList.map((id: string) => allCards.find(x => x.Id === id));
  }
}
