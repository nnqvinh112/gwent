import { Invitation, Player, PlayerStatus } from '@app/shared/models/player';
import { filter, map, tap } from 'rxjs/operators';

import { FirebaseService } from '@app/core/provider/firebase.service';
import { FirestoreService } from './firestore.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QueryFn } from '@angular/fire/firestore';
import { User } from '@app/shared/models/user';

const _ROUTES = {
  PLAYER: 'players',
  BOTS: 'bots',
  INVITATION: 'Invitation',
  INVITATION_SENT: 'SentTo',
  INVITATION_RECEIVED: 'ReceivedFrom'
};

@Injectable()
export class UserService {
  constructor(private firebase: FirebaseService, private firestore: FirestoreService) {}

  getAllPlayers(): Observable<Player[]> {
    const response = this.firestore.get(_ROUTES.PLAYER);
    return response.pipe(map(users => (users || []).map(i => new Player(i))));
  }

  getAllBots(): Observable<Player[]> {
    const response = this.firestore.get(_ROUTES.BOTS);
    return response.pipe(map(users => (users || []).map(i => new Player({ ...i, IsBot: true }))));
  }

  getLobbyPlayers(): Observable<Player[]> {
    return this.getAllPlayers();
  }

  getPlayerById(userId: string): Observable<Player> {
    const queryFn: QueryFn = ref => ref.where('Id', '==', userId);
    const response = this.firestore.get(_ROUTES.PLAYER, queryFn);
    return response.pipe(
      map(res => {
        if (Boolean(res) && res.length > 0) {
          return new Player(res.shift());
        }
        return null;
      })
    );
  }

  sendInvitation(fromUser: User, toUser: User) {
    const sentPath = `${_ROUTES.PLAYER}/${fromUser.Id}/${_ROUTES.INVITATION}/${_ROUTES.INVITATION_SENT}`;
    const receivedPath = `${_ROUTES.PLAYER}/${toUser.Id}/${_ROUTES.INVITATION}/${_ROUTES.INVITATION_RECEIVED}`;
    return Promise.all([this.firebase.push(sentPath, toUser.Id), this.firebase.push(receivedPath, fromUser.Id)]);
  }

  getInvitation(userId: string): Observable<Invitation> {
    const dbList = this.firebase.get(`${_ROUTES.PLAYER}/${userId}/${_ROUTES.INVITATION}`);
    return dbList.snapshotChanges().pipe(
      map(res => {
        if (!res || res.length === 0) {
          return null;
        }

        const output = new Invitation();
        res.forEach(c => {
          const key = c.payload.key;
          const val = c.payload.val();
          if (val instanceof Array) {
            output[key] = val;
          } else {
            output[key] = new Array();
            Object.keys(val).forEach(id => output[key].push(val[id]));
          }
        });
        return output;
      })
    );
  }

  setStatus(userId: string, status: PlayerStatus) {
    return this.firestore.set(_ROUTES.PLAYER, `/${userId}/Status`, status);
  }

  updateUser(user: User) {
    return this.firestore.set(_ROUTES.PLAYER, user.Id, user);
  }

  joinMatch(userId: string, matchId: string) {
    const playerData = { Status: PlayerStatus.IN_MATCH, CurrentMatch: matchId };
    return this.firestore.update(_ROUTES.PLAYER, userId, playerData);
  }
}
