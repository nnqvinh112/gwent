import { QueryFn } from '@angular/fire/firestore';
import { UserService } from '@app/core/provider/user.service';
import { FirestoreService } from './firestore.service';
import { AngularFireList } from 'angularfire2/database';
import { GAMEPLAY } from '@app/shared/constants';
import { FirebaseService } from './firebase.service';
import { Injectable } from '@angular/core';
import { Card, CardRow, Invitation, Player, PlayerStatus, Match, MatchStatus } from '@app/shared/models';
import { Observable, of } from 'rxjs';
import { UUID } from 'angular2-uuid';
import { map } from 'rxjs/operators';

const _ROUTES = {
  MATCH: 'matches',
  PLAYER: 'players',
  STATUS: 'Status'
};

@Injectable()
export class MatchService {
  private _dbList: AngularFireList<any>;
  private _dbKey = '';
  private _match: Match;

  constructor(
    private firebase: FirebaseService,
    private firestore: FirestoreService,
    private userService: UserService
  ) {}

  getMatchById(matchId: string): Observable<Match> {
    const queryFn: QueryFn = (ref) => ref.where('Id', '==', matchId);
    const response = this.firestore.get(_ROUTES.MATCH, queryFn);
    return response.pipe(
      map(matchList => {
        if (!matchList || !matchList.length) {
          throw new Error(`No match found with id ${matchId}`);
        }
        const match = new Match(matchList.shift());
        return match;
      })
    );
  }

  /**
   * Update current turn to opponent's ID of the given player index
   */
  switchTurn(pIndex: number) {
    return this._dbList.update(this._dbKey, {
      CurrentTurn: this._match.Players[+!pIndex].Id,
      Status: MatchStatus[`PLAYER_${+!pIndex + 1}_TURN`]
    });
  }

  pass(pIndex: number, previousValue: boolean[]) {
    const updateData = {
      Status: MatchStatus[`PLAYER_${pIndex + 1}_PASSED`],
      HasPassed: previousValue
    };
    updateData.HasPassed[pIndex] = true;
    return this._dbList.update(this._dbKey, updateData);
  }

  /** Convert player's ID into index between 0 and 1 */
  getPlayerIndex(id: string) {
    for (let i = 0; i < 2; i++) {
      if (this._match.Players[i].Id === id) {
        return i;
      }
    }
    throw new Error(`Unable to find player's index in the current match.
    Player's id: ${id}.
    Current Match's Players: ${JSON.stringify(this._match.Players)}`);
  }

  submitCard(pIndex: number, card: Card) {
    if (!card) {
      throw new Error('Could not submit an undefined card.');
    }
    return Promise.reject();
    // const cardsOnBoard = this._match.CardsOnBoard[pIndex];
    // if (!cardsOnBoard) {
    //   throw new Error('Board rows are not initialized.');
    // }
    // const row = cardsOnBoard.findIndex((item: CardRow) => item.Type.toUpperCase() === card.UnitClass.toUpperCase());

    // if (row < 0 || row >= 3) {
    //   throw new Error(`Invalid card type: ${card.UnitClass}`);
    // }

    // const drawedCards = this._match.DrawedCards;
    // if (drawedCards && drawedCards[pIndex]) {
    //   drawedCards[pIndex].splice(drawedCards[pIndex].indexOf(card.Id), 1);
    // }
    // if (!cardsOnBoard[row].Cards) {
    //   cardsOnBoard[row].Cards = new Array();
    // }
    // (<string[]>cardsOnBoard[row].Cards).push(card.Id);
    // return Promise.all([
    //   this._dbList.set(`${this._dbKey}/DrawedCards`, this._match.DrawedCards),
    //   this._dbList.set(`${this._dbKey}/CardsOnBoard`, this._match.CardsOnBoard)
    // ]);
  }
  /**
   *
   */
  updateStatus(status: string) {
    return this.firebase
      .set(`${_ROUTES.MATCH}/${this._match.Id}/${_ROUTES.STATUS}`, status)
      .catch(error => console.error(error));
  }
  /**
   * Add the winnder ID to the result list
   */
  finishRound(winnerID?: string) {
    this._match.Result = this._match.Result || [];
    this._match.Result.push(winnerID || 'undefined');
    return this._dbList.update(this._dbKey, { Result: this._match.Result, Status: MatchStatus.RoundEnded });
  }
  /**
   * To start a new round:
   * #1 Move on-board cards to discarded cards
   * #2 Reset HasPassed variable
   * #3 Switch first turn
   * #4 Increase round number
   * @returns new match
   */
  startNewRound(): Promise<Match> {
    return Promise.reject();
    // this._match.CurrentRound += 1;
    // this._match.CardsOnBoard.forEach((cardRows, pIndex) => {
    //   cardRows.forEach(row => {
    //     if (row.Cards) {
    //       if (!this._match.Discardedcards) {
    //         this._match.Discardedcards = [[], []];
    //       }
    //       if (!this._match.Discardedcards[pIndex]) {
    //         this._match.Discardedcards[pIndex] = [];
    //       }
    //       this._match.Discardedcards[pIndex].push(...(<any>row.Cards));
    //     }
    //     row.Cards = new Array(); // Empty cards on board
    //   });
    // });
    // this._match.HasPassed = [false, false];
    // this._match.Status = MATCH_STATUS.INITIALIZE_ROUND;
    // const index = this.getPlayerIndex(this._match.FirstTurn);
    // this._match.FirstTurn = this._match.CurrentTurn = this._match.Players[+!index].Id;
    // return this._dbList.update(this._dbKey, this._match).then(() => this._match);
  }

  createMatch(player1: Player, player2: Player) {
    const match = Match.initializeNewMatch(player1, player2);
    const requests = [this.firestore.set(_ROUTES.MATCH, match.Id, match)];
    if (!player1.IsBot) {
      requests.push(this.userService.joinMatch(player1.Id, match.Id));
    }
    if (!player2.IsBot) {
      requests.push(this.userService.joinMatch(player2.Id, match.Id));
    }
    return Promise.all(requests).then(() => match);
  }

  /**
   * Clear data created while playing, only keep the result.
   */
  finishMatch(match: Match, pIndex?: number) {
    const updateData = { Status: PlayerStatus.IN_LOBBY, CurrentMatch: <Match>null };
    // Clear match data to reduce data on server
    // match.CardsInDeck = null;
    // match.CardsOnBoard = null;
    // match.Discardedcards = null;
    // match.DrawedCards = null;
    match.Status = MatchStatus.MatchEnded;
    return Promise.all([
      this.firebase.update(`${_ROUTES.PLAYER}/${match.Players[0].Id}`, updateData),
      this.firebase.update(`${_ROUTES.PLAYER}/${match.Players[1].Id}`, updateData),
      this.firebase.update(`${_ROUTES.MATCH}/${match.Id}`, match)
    ]);
  }

  /**
   * Assign random cards from the player's chosen deck to the match.
   * Then draw random cards from the assigned list and assign to the player's drawed cards
   */
  assignDeck(playerId: string, cardIdList?: string[]) {
    const pIndex = this.getPlayerIndex(playerId);
    // if (!this._match.CardsInDeck) {
    //   this._match.CardsInDeck = [[], []];
    // }

    // const deck = cardIdList;
    // const drawed = this.randomizeCards(deck, GAMEPLAY.NUM_CARD_FIRST_DRAW);

    // // Remove drawed cards from deck
    // drawed.forEach(i => {
    //   deck.splice(deck.findIndex(x => x === i), 1);
    // });

    // this._match.CardsInDeck[pIndex] = deck;
    // this._match.DrawedCards[pIndex] = drawed;
    return this.firebase.update(`${_ROUTES.MATCH}/${this._match.Id}`, this._match);
  }

  /**
   * Take a random list of card id list out of a given id list
   */
  randomizeCards(cardIdList: string[], length: number) {
    const inputArray = [...cardIdList];
    const outputArray: string[] = [];
    for (let i = 0; i < length; i += 1) {
      // Random number between 0 and inputArray.length - 1
      const randomIndex = Math.floor(Math.random() * (inputArray.length - 1));
      const cardId = inputArray[randomIndex];
      outputArray.push(cardId);
      inputArray.splice(randomIndex, 1);
    }
    return outputArray;
  }
}
