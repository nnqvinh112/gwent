import { CardService } from './card.service';
import { ConverterService } from './converter.service';
import { DeckService } from './deck.service';
import { FirebaseService } from './firebase.service';
import { FirestoreService } from './firestore.service';
import { LobbyService } from './lobby.service';
import { MatchService } from './match.service';
import { NgModule } from '@angular/core';
import { NoticeComponent } from '@app/shared/components/notice/notice.component';
import { NoticeService } from './notice.service';
import { UserService } from './user.service';

@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    CardService,
    FirebaseService,
    MatchService,
    NoticeService,
    UserService,
    FirestoreService,
    LobbyService,
    ConverterService,
    DeckService
  ],
  entryComponents: [NoticeComponent]
})
export class ProviderModule {}
