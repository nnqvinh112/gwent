import { AngularFirestore, QueryFn } from '@angular/fire/firestore';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';

@Injectable()
export class FirestoreService {
  constructor(private firestore: AngularFirestore) {}

  get(collection: string, queryFn?: QueryFn): Observable<any[]> {
    return this.firestore
      .collection(collection, queryFn)
      .snapshotChanges()
      .pipe(map(actions => (actions || []).map(i => i.payload.doc.data())));
  }

  push(collection: string, data: any) {
    return this.firestore.collection(collection).add(this.json(data));
  }

  set(collection: string, docPath: string, data: any) {
    return this.firestore
      .collection(collection)
      .doc(docPath)
      .set(this.json(data));
  }

  update(collection: string, docPath: string, data: any) {
    return this.firestore
      .collection(collection)
      .doc(docPath)
      .update(this.json(data));
  }

  json(data: any) {
    return JSON.parse(JSON.stringify(data));
  }
}
