import { EventEmitter, Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Card } from '@app/shared/models/card';
import { FirestoreService } from './firestore.service';
import { Store } from '@ngxs/store';

const _ROUTES = {
  ALL: `cards`
};

@Injectable()
export class CardService {
  globalCards: Card[] = [];
  hoveredCard = new EventEmitter<Card>();

  constructor(private firestore: FirestoreService, private store: Store) {}

  getAllCards(): Observable<Card[]> {
    return this.firestore.get(_ROUTES.ALL).pipe(
      map(res => {
        const cards = (res || []).map(i => new Card(i));
        return cards;
      })
    );
  }
}
