import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  EventEmitter,
  Injectable,
  Injector
} from '@angular/core';
import { NoticeComponent } from '@app/shared/components/notice/notice.component';

const _KEY = {
  LOADER: 'showLoader',
  MESSAGE: 'message',
  SHOW: 'show'
};
const _INTERVAL = {
  TIME: 3 // time in milisecond
};

export class NoticeOption {
  showLoader?: boolean;
  timeout?: number;
  allowDuplicate?: boolean;
}
export class NoticeMessage {
  message: string;
  option?: NoticeOption;
}

@Injectable()
export class NoticeService {
  get isShowing() {
    return this._notices.length > 0 || this._componentRef.instance[_KEY.SHOW];
  }
  get remainingMessage() {
    return this._notices;
  }
  get currentMessage() {
    return this._current;
  }
  private _notices: NoticeMessage[] = [];
  private _componentRef: ComponentRef<{}>;
  private _promsie: Promise<any>;
  private _timer: any;
  private _current: NoticeMessage;

  /**
   * Event when all messages have finished
   */
  finished = new EventEmitter<NoticeMessage>();
  /**
   * Event when a message has finished
   */
  closed = new EventEmitter<NoticeMessage>();

  constructor(
    private compFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector
  ) {
    this.appendComponent(NoticeComponent);
  }
  private appendComponent(component: any) {
    // 1. Create a component reference from the component
    this._componentRef = this.compFactoryResolver.resolveComponentFactory(component).create(this.injector);

    // 2. Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(this._componentRef.hostView);

    // 3. Get DOM element from component
    const domElem = (this._componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    // 4. Append DOM element to the body
    document.body.appendChild(domElem);
  }
  /**
   * Push the message(s) to the queue and show it sequentially
   * @param message message(s) to push into the queue
   * @param showLoader show spinning cog
   * @param timeout time to auto hide, if this value <= 0, the notcie will not hide until it is hidden manually
   */
  show(message: string | string[], option?: NoticeOption): void {
    if (message instanceof Array) {
      this._notices.push(...message.map(x => ({ message: x, option } as NoticeMessage)));
    } else {
      const allowDuplicate = option ? option.allowDuplicate : false;
      if (allowDuplicate || (!allowDuplicate && !this._notices.find(x => x.message === message))) {
        this._notices.push({ message, option });
      }
    }
    this.startTimer();
  }
  private startTimer() {
    if (!this._timer) {
      this._timer = setInterval(() => this.interval(), _INTERVAL.TIME);
    }
  }
  private interval() {
    if (this._notices.length > 0 && !this._current) {
      this._current = this._notices.shift();
      if (this._current) {
        this.showSingleMessage(this._current.message, this._current.option).then(() =>
          // Wait 0.3s for fade out animation
          this.delay(300).then(() => {
            this.closed.emit(this._current);
            if (this._notices.length === 0) {
              this.finished.emit(this._current);
            }
            this._current = null;
          })
        );
      } else {
        clearInterval(this._timer);
      }
    }
  }
  /**
   * Wait for specific time and return
   * @param time time to delay in milisecond
   */
  private delay(time: number): Promise<void> {
    return (this._promsie = new Promise(resolve => {
      setTimeout(resolve.bind(null, null), time);
    }));
  }
  private showSingleMessage(message: string, option?: NoticeOption): Promise<void> {
    // console.log('[Notice] ', message);
    this._componentRef.instance[_KEY.LOADER] = option ? option.showLoader : false;
    this._componentRef.instance[_KEY.MESSAGE] = message;
    this._componentRef.instance[_KEY.SHOW] = true;
    const timeout = option ? option.timeout : -1;
    if (!timeout || timeout <= 0) {
      return Promise.resolve();
    }
    return this.delay(timeout).then(() => this.hide());
  }

  /**
   * Hide the notice immediately without clearing the queue
   */
  hide() {
    this._componentRef.instance[_KEY.SHOW] = false;
  }
  /**
   * Clear current message and all remaining messages
   */
  clear() {
    this._notices = [];
    this._current = null;
  }
  /**
   * Skip the current message and go to the next message
   */
  next() {}
}
